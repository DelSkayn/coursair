use super::schema::*;

#[derive(Identifiable,Associations,Queryable,Serialize, Deserialize,Clone)]
#[has_many(comments)]
#[has_many(users_likes)]
pub struct User{
    pub id: i32,
    pub name: String,
    pub admin: bool,
    pub password: i64,
    pub salt: i64,
}

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser<'a>{
    pub name: &'a str,
    pub password: i64,
    pub salt: i64,
    pub admin: bool,
}


#[derive(Identifiable,Associations,Queryable,Serialize, Deserialize,Clone)]
#[has_many(has_tags)]
pub struct Tag{
    pub id: i32,
    pub name: String,
    pub explenation: Option<String>,
}


#[derive(Identifiable,Associations,Queryable,Serialize, Deserialize,Clone)]
#[has_many(has_tags)]
#[has_many(comments)]
#[has_many(users_likes)]
pub struct Course{
    pub id: i32,
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year : String,
    pub docenten : String,
    pub ec : String,
    pub niveau : String,
    pub summary: Option<String>,
    pub discription: Option<String>,
}

#[derive(Insertable)]
#[table_name="courses"]
pub struct NewCourses{
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year : String,
    pub docenten : String,
    pub ec : String,
    pub niveau : String,
    pub summary: Option<String>,
    pub discription: Option<String>,
}

#[derive(Identifiable,Associations,Queryable,Serialize, Deserialize,Clone)]
#[belongs_to(users)]
#[belongs_to(courses)]
pub struct Comment{
    pub id: i32,
    pub user_id: i32,
    pub course_id: i32,
    pub content: String,
}

#[derive(Insertable)]
#[table_name="comments"]
pub struct NewComment<'a>{
    pub user_id: i32,
    pub course_id: i32,
    pub content: &'a str,
}

#[derive(Identifiable,Associations,Queryable,Serialize, Deserialize,Clone)]
#[belongs_to(Course)]
#[belongs_to(Tag)]
pub struct HasTag{
    pub id: i32,
    pub tag_id: i32,
    pub course_id: i32,
    pub strength: i32,
}

#[derive(Identifiable,Associations,Queryable,Serialize, Deserialize,Clone)]
#[belongs_to(Course)]
#[belongs_to(User)]
pub struct UsersLike{
    pub id: i32,
    pub user_id: i32,
    pub course_id: i32,
}

#[derive(Insertable)]
#[table_name="users_likes"]
pub struct NewUsersLike{
    pub user_id: i32,
    pub course_id: i32,
}

