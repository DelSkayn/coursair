#![feature(proc_macro,custom_attribute)]
#![allow(dead_code)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel_codegen;
#[macro_use]
extern crate horrorshow;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate iron;
extern crate serde;
extern crate serde_json;
extern crate dotenv;
extern crate mount;
extern crate staticfile;
extern crate urlencoded;
extern crate rand;

use horrorshow::prelude::*;
use diesel::migrations;
use dotenv::dotenv;
use iron::prelude::*;
use iron::headers::ContentType;
use staticfile::Static;
use mount::Mount;

use std::env;
use std::path::Path;
use std::sync::Mutex;
use std::sync::Arc;

pub mod db;
pub mod logger;
use logger::Logger;
mod session;
pub use session::{Session,SessionData};
mod coursair_user;
use coursair_user::CoursairUser;
mod courses;
use courses::Courses;
mod register;
use register::Register;
mod login;
use login::Login;
mod detail;
use detail::Detail;
mod like;
use like::Like;
mod is_liked;
use is_liked::IsLiked;
mod refresh;
use refresh::Refresh;
mod single;
use single::Single;
mod new_course;
use new_course::NewCourse;
mod new_comment;
use new_comment::NewComments;
mod comments;
use comments::Comments;
mod alter;
use alter::Alter;
mod filters;
use filters::Filter;


static MATERIAL_ICONS_LINK: &'static str = r#"https://fonts.googleapis.com/icon?family=Material+Icons"#;
static FONT_LINK: &'static str = r#"https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300|Rubik"#;

fn index(_: &mut Request) -> IronResult<Response>{
    let res = html!{
        html {
            head  {
                meta(charset="UTF-8") {}
                link (href=MATERIAL_ICONS_LINK,rel="stylesheet"){}
                link (href=FONT_LINK,rel="stylesheet"){}
                link (href="style/index.css",rel="stylesheet"){}
                script (src="target/index.js") {}
            }
            body{
                script { : "Elm.Main.fullscreen()"}
            }
        }
    }.into_string().unwrap();
    let mut resp = Response::new();
    resp.body = Some(Box::new(res));
    resp.status = Some(iron::status::Ok);
    resp.headers.set(ContentType::html());
    Ok(resp)
}

fn editor(_: &mut Request) -> IronResult<Response>{
    let res = html!{
        html {
            head  {
                meta(charset="UTF-8") {}
                link (href=MATERIAL_ICONS_LINK,rel="stylesheet"){}
                link (href=FONT_LINK,rel="stylesheet"){}
                link (href="style/index.css",rel="stylesheet"){}
                script (src="target/add_course.min.js") {}
            }
            body{
                script { : "Elm.Main.fullscreen()"}
            }
        }
    }.into_string().unwrap();
    let mut resp = Response::new();
    resp.body = Some(Box::new(res));
    resp.status = Some(iron::status::Ok);
    resp.headers.set(ContentType::html());
    Ok(resp)
}

mod courses;
use courses::Courses;
mod register;
use register::Register;

use std::sync::Arc;

fn main(){
    Logger::init().unwrap();
    dotenv().ok();

    let database = Arc::new(Mutex::new(db::establish_connection()));
    let server_ip = env::var("COURSAIR_SERVER_IP")
        .expect("COURSAIR_SERVER_IP must be set");

    migrations::run_pending_migrations(&*database.lock().unwrap()).unwrap();

    info!("Running server on: \"{}\"",server_ip);

    let session = Session::new();

    let mut mount = Mount::new();

    mount.mount("/data/courses/", Courses::new(database.clone()));
    mount.mount("/data/register/", Register::new(database.clone()));
    mount.mount("/data/login/", Login::new(database.clone(),session.clone()));
    mount.mount("/data/like/", Like::new(database.clone(),session.clone()));
    mount.mount("/data/is_liked/", IsLiked::new(database.clone(),session.clone()));
    mount.mount("/data/refresh/", Refresh::new(session.clone()));
    mount.mount("/data/user/", CoursairUser::new(database.clone(),session.clone()));
    mount.mount("/data/filter/", Filter::new(database.clone()));
    mount.mount("/data/new_comment/", NewComments::new(database.clone(),session.clone()));
    mount.mount("/data/comments/", Comments::new(database.clone()));

    mount.mount("/data/alter", Alter::new(database.clone(),session.clone()));
    mount.mount("/data/single/", Single::new(database.clone(),session.clone()));
    mount.mount("/data/new_course", NewCourse::new(database.clone(),session.clone()));
    mount.mount("/editor", editor);

    mount.mount("/course", Detail::new(database.clone()));
    mount.mount("/style", Static::new(Path::new("front/style")));
    mount.mount("/target", Static::new(Path::new("front/target")));
    mount.mount("/res", Static::new(Path::new("front/res")));
    mount.mount("/", index);
    mount.mount("/index", index);
    Iron::new(mount).http(server_ip.as_str()).unwrap();
}
