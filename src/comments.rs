use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel;
use super::diesel::pg::expression::dsl::any;
use super::diesel::expression::sql_literal::SqlLiteral;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;


use super::db::schema::courses::dsl::{courses,year,niveau,language,ec,periode,name};
use super::db::schema::has_tags::dsl::{course_id,tag_id,has_tags};
use super::db::schema::tags::dsl::{tags,id};
use super::db::models::Comment;
use super::db::models::Tag;

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;
use std::io::Read;

pub struct Comments {
    connection: Arc<Mutex<PgConnection>>,
}

#[derive(Deserialize)]
pub struct CommentRequest{
    pub course_id: i32,
}

#[derive(Serialize)]
pub struct CommentR{
    pub username: String,
    pub content: String,
}

#[derive(Serialize)]
pub struct CommentResult {
    pub comments: Vec<CommentR>,
}

impl Comments {
    pub fn new(connection: Arc<Mutex<PgConnection>>) -> Self {
        Comments { connection: connection }
    }
}

impl Handler for Comments {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        debug!("course request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("course body: {}",body);
        let course_req: CommentRequest = itry!(serde_json::from_str(&body));

        let query = format!("U.name, C.content FROM
comments as C
JOIN users as U
on C.user_id = U.id
WHERE C.course_id = {}", course_req.course_id);

        let mut comments: Vec<(String,String)>
            = itry!(diesel::select(SqlLiteral::new(query))
                    .load(&*self.connection.lock().unwrap()));

        let res = CommentResult{
            comments: comments.drain(..)
                .map(|e| CommentR{ username: e.0,content: e.1 })
                .collect(),
        };

        println!("{}",serde_json::to_string_pretty(&res).unwrap());

        Ok(Response::with((status::Ok, serde_json::to_string_pretty(&res).unwrap())))
    }
}
