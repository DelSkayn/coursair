use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::{Session};

use super::db::models::NewUsersLike;
use super::db::schema::users_likes as db_users_likes;
use super::db::schema::users_likes::dsl::{users_likes, course_id, user_id};

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;

use std::io::Read;


pub struct Like{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Serialize)]
pub struct LikeResponse<'a>{
    succesfull: bool,
    error: Option<&'a str>,
}

#[derive(Deserialize)]
pub struct LikeRequest{
    like: bool,
    session_id: u64,
    course_id: u64,
}


unsafe impl Send for Like{}
unsafe impl Sync for Like{}

impl Like{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        Like{
            connection: connection,
            session: session,
        }
    }
}

impl Handler for Like{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("like request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("like request body: {}",body);
        let like_req: LikeRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LikeResponse{
                    succesfull: false,
                    error: Some("Bad request"),
                }).unwrap())));
            }
        };

        let sess = match self.session.get(like_req.session_id){
            Some(x) => x,
            None => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LikeResponse{
                    succesfull: false,
                    error: Some("Not logged in."),
                }).unwrap())));
            },
        };

        if like_req.like{
            let amount: i64 = itry!(users_likes.filter(course_id.eq(like_req.course_id as i32))
                .filter(user_id.eq(sess.id as i32))
                .count()
                .first(&*self.connection.lock().unwrap()));

            if amount > 0{
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LikeResponse{
                    succesfull: false,
                    error: Some("User already like course."),
                }).unwrap())))
            }

            let new_like = NewUsersLike{
                user_id: sess.id as i32,
                course_id: like_req.course_id as i32,
            };
            itry!(diesel::insert(&new_like)
                  .into(db_users_likes::table)
                  .execute(&*self.connection.lock().unwrap()));

            Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LikeResponse{
                succesfull: true,
                error: None,
            }).unwrap())))
        }else{
            itry!(diesel::delete(users_likes.filter(course_id.eq(like_req.course_id as i32))
                .filter(user_id.eq(sess.id as i32)))
                .execute(&*self.connection.lock().unwrap()));
            Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LikeResponse{
                succesfull: true,
                error: None,
            }).unwrap())))
        }
    }
}
