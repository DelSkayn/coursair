use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::{Session,SessionData};

use super::db::models::User;
use super::db::schema::users::dsl::*;

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;
use std::hash::{Hasher,Hash};
use std::collections::hash_map::DefaultHasher;

use std::io::Read;


pub struct Login{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Serialize)]
pub struct LoginResponse<'a>{
    succesfull: bool,
    error: Option<&'a str>,
    session_id: Option<u64>,
}

#[derive(Deserialize)]
pub struct LoginRequest{
    username: String,
    password: String,
}

unsafe impl Send for Login{}
unsafe impl Sync for Login{}

impl Login{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        Login{
            connection: connection,
            session: session,
        }
    }
}

impl Handler for Login{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("login request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("login request body: {}",body);
        let log_req: LoginRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LoginResponse{
                    succesfull: false,
                    error: Some("Bad request"),
                    session_id: None,
                }).unwrap())));
            }
        };

        let user = itry!(users.filter(name.eq(log_req.username.clone()))
            .load::<User>(&*self.connection.lock().unwrap()));

        if user.len() == 0{
            return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LoginResponse{
                succesfull: false,
                error: Some("Invalid password or username"),
                session_id: None,
            }).unwrap())));
        }

        let mut hasher = DefaultHasher::new();
        log_req.password.hash(&mut hasher);
        hasher.write_i64(user[0].salt);
        let password_hash = hasher.finish() as i64;
        if password_hash == user[0].password{
            debug!("Login succesfull!");
            let session_id = self.session.put(SessionData{
                id: user[0].id,
                admin: user[0].admin,
            });
            Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LoginResponse{
                succesfull: true,
                error: None, 
                session_id: Some(session_id),
            }).unwrap())))
        }else{
            Ok(Response::with((status::Ok,serde_json::to_string_pretty(&LoginResponse{
                succesfull: false,
                error: Some("Invalid password or username"), 
                session_id: None,
            }).unwrap())))
        }

    }
}
