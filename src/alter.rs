use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::Session;

use super::db::schema::courses::dsl::*;

use std::io::Read;

use super::serde_json;


use std::sync::Arc;
use std::sync::Mutex;

pub struct Alter{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Deserialize)]
pub struct AlterRequest{
    pub id: i32,
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year: String,
    pub docenten: String,
    pub ec: String,
    pub niveau: String,
    pub discription: String,
    pub summary: String,
} 
#[derive(Serialize)]
pub struct AlterResponse{
    succesfull: bool,
}

unsafe impl Send for Alter{}
unsafe impl Sync for Alter{}

impl Alter{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        Alter{
            connection: connection,
            session: session,
        }
    }
}

impl Handler for Alter{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("login request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("login request body: {}",body);
        let alter_req: AlterRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&AlterResponse{
                    succesfull: false,
                }).unwrap())));
            }
        };

        itry!(diesel::update(courses.filter(id.eq(alter_req.id)))
            .set((name.eq(alter_req.name)
            , language.eq(alter_req.language)
            , periode.eq(alter_req.periode)
            , year.eq(alter_req.year)
            , docenten.eq(alter_req.docenten)
            , ec.eq(alter_req.ec)
            , niveau.eq(alter_req.niveau)
            , discription.eq(alter_req.discription)
            , summary.eq(alter_req.summary)))
            .execute(&*self.connection.lock().unwrap()));

        return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&AlterResponse{
            succesfull: false,
        }).unwrap())));

    }
}
