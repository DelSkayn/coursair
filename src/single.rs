use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::Session;

use super::db::schema::courses::dsl::{courses, id};
use super::db::models::Course;
use super::db::models::Tag;

use std::io::Read;

use super::serde_json;


use std::sync::Arc;
use std::sync::Mutex;



pub struct Single{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Deserialize)]
pub struct SingleRequest{
    course_id: i32,
}

#[derive(Serialize)]
pub struct SingleResponse{
    succesfull: bool,
    course: Option<CourseResult>,
}

#[derive(Serialize)]
pub struct CourseResult {
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year: String,
    pub docenten: String,
    pub ec: String,
    pub niveau: String,
    pub discription: String,
    pub summary: String,
    pub user_liked: bool,
    pub tags: Vec<Tag>,
}

unsafe impl Send for Single{}
unsafe impl Sync for Single{}

impl Single{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        Single{
            connection: connection,
            session: session,
        }
    }
}

impl Handler for Single{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("login request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("login request body: {}",body);
        let log_req: SingleRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&SingleResponse{
                    succesfull: false,
                    course: None,
                }).unwrap())));
            }
        };

        let mut result = itry!(courses.filter(id.eq(log_req.course_id))
                                   .load::<Course>(&*self.connection.lock().unwrap()),
                                       status::InternalServerError);

        if result.len() > 0 {
            let new_course = CourseResult{
                name: result[0].name.clone(),
                language: result[0].language.clone(),
                periode: result[0].periode.clone(),
                year: result[0].year.clone(),
                docenten: result[0].docenten.clone(),
                ec: result[0].ec.clone(),
                niveau: result[0].niveau.clone(),
                discription: result[0].discription.take().unwrap_or("".to_string()),
                summary: result[0].summary.take().unwrap_or("".to_string()),
                user_liked: false,
                tags: Vec::new(),
            };
            return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&SingleResponse{
                succesfull: true,
                course: Some(new_course),
            }).unwrap())));
        }else{
            return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&SingleResponse{
                succesfull: false,
                course: None,
            }).unwrap())));
        }
    }
}
