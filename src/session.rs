use std::sync::{Arc, RwLock};
use std::net::SocketAddr;
use std::sync::atomic::{Ordering, AtomicUsize};

use std::collections::HashMap;



#[derive(Clone,Debug)]
pub struct SessionData {
    pub id: i32,
    pub admin: bool,
}

#[derive(Debug)]
struct Data {
    data: RwLock<HashMap<u64, SessionData>>,
    next_id: AtomicUsize,
}

#[derive(Hash,Eq,PartialEq,Debug)]
struct Id {
    remote: SocketAddr,
    local: SocketAddr,
}

#[derive(Clone,Debug)]
pub struct Session {
    inner: Arc<Data>,
}

impl Session {
    pub fn new() -> Self {
        Session {
            inner: Arc::new(Data {
                data: RwLock::new(HashMap::new()),
                next_id: AtomicUsize::new(0),
            }),
        }
    }

    pub fn get(&self, id: u64) -> Option<SessionData> {
        self.inner
            .data
            .read()
            .unwrap()
            .get(&id)
            .map(|e| e.clone())
    }

    pub fn adjust(&self, id: u64, s: SessionData) {
        self.inner
            .data
            .write()
            .unwrap()
            .insert(id, s);
    }

    pub fn put(&self, s: SessionData) -> u64 {
        let new_id = self.inner.next_id.fetch_add(1, Ordering::AcqRel) as u64;
        self.inner
            .data
            .write()
            .unwrap()
            .insert(new_id, s);
        new_id
    }
}
