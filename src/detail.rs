use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel;
use super::diesel::expression::sql_literal::SqlLiteral;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;
use super::iron::headers::ContentType;
use super::horrorshow::prelude::*;

use super::db::schema::courses::dsl::{courses, id};
use super::db::schema::has_tags::dsl::{tag_id,has_tags};
use super::db::schema::tags::dsl::{tags,self};
use super::db::models::Course;
use super::db::models::Tag;

use super::serde_json;

use ::urlencoded::UrlEncodedQuery;

use std::sync::Arc;
use std::sync::Mutex;


use super::{MATERIAL_ICONS_LINK, FONT_LINK};

pub struct Detail {
    connection: Arc<Mutex<PgConnection>>,
}

#[derive(Serialize)]
pub struct OtherCourses{
    pub id: i32,
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year: String,
    pub docenten: String,
    pub ec: String, 
    pub niveau: String,
}

#[derive(Serialize)]
pub struct CourseResult {
    pub id: i32,
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year: String,
    pub docenten: String,
    pub ec: String,
    pub niveau: String,
    pub discription: String,
    pub summary: String,
    pub user_liked: bool,
    pub tags: Vec<Tag>,
    pub other_courses: Vec<OtherCourses>,
}

unsafe impl Send for Detail {}
unsafe impl Sync for Detail {}

fn page(data: String) -> String {
    (html!{
        html {
            head  {
                meta(charset="UTF-8") {}
                link (href=MATERIAL_ICONS_LINK,rel="stylesheet"){}
                link (href=FONT_LINK,rel="stylesheet"){}
                link (href="style/index.css",rel="stylesheet"){}
                script (src="target/course.js") {}
            }
            body{
                script { : Raw(format!("Elm.Main.fullscreen(\'{}\')",data))}
            }
        }
    })
        .into_string()
        .unwrap()
}

impl Detail {
    pub fn new(connection: Arc<Mutex<PgConnection>>) -> Self {
        Detail { connection: connection }
    }
}

impl Handler for Detail {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        let encoded = match req.get_ref::<UrlEncodedQuery>() {
            Ok(x) => x,
            Err(_) => {
                // What
                return Ok(Response::with(status::ImATeapot));
            }
        };
        let c_id: i32 = if let Some(x) = encoded.get("id") {
            match x[0].parse() {
                Ok(x) => x,
                Err(_) => {
                    return Ok(Response::with(status::ImATeapot));
                }
            }
        } else {
            return Ok(Response::with(status::ImATeapot));
        };
        let mut result = itry!(courses.filter(id.eq(c_id))
                                   .load::<Course>(&*self.connection.lock().unwrap()),
                               status::InternalServerError);

        if result.len() > 0 {
            let needed_tags: Vec<i32> = itry!(courses
                .inner_join(has_tags)
                .filter(id.eq(c_id))
                .select((tag_id))
                .load(&*self.connection.lock().unwrap()));

            let loaded_tags: Vec<Tag> = if needed_tags.len() > 0{
                itry!(tags.filter(dsl::id.eq_any(&needed_tags)) .load(&*self.connection.lock().unwrap()))
            }else{
                Vec::new()
            };

            let query = format!("L2.course_id, count(L2.user_id) as num_users 
from users_likes L1
join users_likes L2
on L1.course_id={}
and L2.user_id = L1.user_id
and L2.course_id <> {}
group by L2.course_id
order by num_users desc
LIMIT 3;",c_id,c_id);

            let mut re_courses: Vec<(i32,i64)> 
                = itry!(diesel::select(SqlLiteral::new(query))
                        .load(&*self.connection.lock().unwrap()));

            let cour: Vec<i32> = re_courses.drain(..)
                .map(|x| x.0)
                .collect();

            let mut other_courses:Vec<Course> = if cour.len() > 0 {
                itry!(courses.filter(id.eq_any(&cour)).load(&*self.connection.lock().unwrap()))
            }else {
                Vec::new()
            };

            let new_course = CourseResult{
                id: result[0].id.clone(),
                name: result[0].name.clone(),
                language: result[0].language.clone(),
                periode: result[0].periode.clone(),
                year: result[0].year.clone(),
                docenten: result[0].docenten.clone(),
                ec: result[0].ec.clone(),
                niveau: result[0].niveau.clone(),
                discription: result[0].discription.take().map(|e| e.replace("\n", "\\n")).unwrap_or("Missing description!".to_string()),
                summary: result[0].summary.take().map(|e| e.replace("\n", "\\n")).unwrap_or("Missing summary!".to_string()),
                user_liked: false,
                other_courses: other_courses.drain(..).map(|x|{
                    OtherCourses{
                        id: x.id,
                        name: x.name,
                        language: x.language,
                        periode: x.periode,
                        year: x.year,
                        docenten: x.docenten,
                        ec: x.ec, 
                        niveau: x.niveau,
                    }
                }).collect(),
                tags: loaded_tags,
            };

            let data = serde_json::to_string(&new_course).unwrap();
            let mut res = Response::with((status::Ok, page(data)));
            res.headers.set(ContentType::html());
            Ok(res)

        } else {
            Ok(Response::with(status::NotFound))
        }
    }
}
