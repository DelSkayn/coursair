use super::diesel::pg::PgConnection;
use super::diesel;
use super::diesel::prelude::*;
use super::diesel::pg::expression::dsl::any;
use super::diesel::expression::sql_literal::SqlLiteral;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;


use super::db::schema::tags::dsl::{tags, id};
use super::db::schema::comments as db_comments;
use super::db::models::{NewComment, Comment};
use super::db::models::Tag;

use super::serde_json;
use super::Session;

use std::sync::Arc;
use std::sync::Mutex;
use std::io::Read;

pub struct NewComments {
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Deserialize)]
pub struct NewCommentRequest {
    pub course_id: u64,
    pub session_id: u64,
    pub content: String,
}

#[derive(Serialize)]
pub struct NewCommentResult {
    pub succesfull: bool,
}

impl NewComments {
    pub fn new(connection: Arc<Mutex<PgConnection>>, session: Session) -> Self {
        NewComments {
            connection: connection,
            session: session,
        }
    }
}

impl Handler for NewComments {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        debug!("course request: {:#?}", req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("course body: {}", body);
        let new_comment_req: NewCommentRequest = itry!(serde_json::from_str(&body));

        let user_id = match self.session.get(new_comment_req.session_id) {
            Some(x) => x,
            None => {
                return Ok(Response::with((status::Ok,
                              serde_json::to_string_pretty(&NewCommentResult{
                                  succesfull: false,
                              }).unwrap())));
            }
        };

        let new_comment = NewComment {
            user_id: user_id.id as i32,
            course_id: new_comment_req.course_id as i32,
            content: &new_comment_req.content,
        };

        itry!(diesel::insert(&new_comment)
            .into(db_comments::table)
            .execute(&*self.connection.lock().unwrap()));

        let res = NewCommentResult { succesfull: true };
        Ok(Response::with((status::Ok, serde_json::to_string_pretty(&res).unwrap())))
    }
}
