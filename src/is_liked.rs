use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::Session;

use super::db::schema::users_likes::dsl::{users_likes, course_id, user_id};

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;

use std::io::Read;


pub struct IsLiked{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Serialize)]
pub struct IsLikedResponse<'a>{
    succesfull: bool,
    error: Option<&'a str>,
    is_liked: Option<bool>,
}

#[derive(Deserialize)]
pub struct IsLikedRequest{
    session_id: u64,
    course_id: u64,
}


unsafe impl Send for IsLiked{}
unsafe impl Sync for IsLiked{}

impl IsLiked{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        IsLiked{
            connection: connection,
            session: session,
        }
    }
}

impl Handler for IsLiked{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("is liked request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("like request body: {}",body);
        let like_req: IsLikedRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&IsLikedResponse{
                    succesfull: false,
                    error: Some("Bad request"),
                    is_liked: None,
                }).unwrap())));
            }
        };

        

        let sess = match self.session.get(like_req.session_id){
            Some(x) => x,
            None => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&IsLikedResponse{
                    succesfull: false,
                    error: Some("Not logged in."),
                    is_liked: None,
                }).unwrap())));
            },
        };

            let amount: i64 = itry!(users_likes.filter(course_id.eq(like_req.course_id as i32))
                .filter(user_id.eq(sess.id as i32))
                .count()
                .first(&*self.connection.lock().unwrap()));

            if amount > 0{
                Ok(Response::with((status::Ok,serde_json::to_string_pretty(&IsLikedResponse{
                    succesfull: true,
                    error: None,
                    is_liked: Some(true),
                }).unwrap())))
            }else{
                Ok(Response::with((status::Ok,serde_json::to_string_pretty(&IsLikedResponse{
                    succesfull: true,
                    error: None,
                    is_liked: Some(false),
                }).unwrap())))
            }

    }
}
