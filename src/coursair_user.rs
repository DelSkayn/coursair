use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::Session;

use super::db::models::User;
use super::db::schema::users::dsl::*;

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;

use std::io::Read;


pub struct CoursairUser{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}


#[derive(Serialize)]
pub struct UserData{
    name: String,
    admin: bool,
}

#[derive(Deserialize)]
pub struct CoursairUserRequest{
    session_id: u64,
}

#[derive(Serialize)]
pub struct CoursairUserResponse<'a>{
    succesfull: bool,
    error: Option<&'a str>,
    data: Option<UserData>,
}

impl CoursairUser{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        CoursairUser{
            connection: connection,
            session: session
        }
    }
}

impl Handler for CoursairUser{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("User request body: {}",body);
        let log_req: CoursairUserRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&CoursairUserResponse{
                    succesfull: false,
                    error: Some("Bad request"),
                    data: None,
                }).unwrap())));
            }
        };
        let ses = if let Some(x) = self.session.get(log_req.session_id){
            x
        }else{
            return Ok(Response::with((status::Ok,serde_json::to_string(&CoursairUserResponse{
                succesfull: false,
                error: Some("Not logged in!"),
                data: None,
            }).unwrap())))
        };
        let user = itry!(users.filter(id.eq(ses.id))
                         .load::<User>(&*self.connection.lock().unwrap()));
        if user.len() == 0{
            panic!("Invalid user id in session");
        }

        Ok(Response::with((status::Ok,serde_json::to_string(&CoursairUserResponse{
            succesfull: true,
            error: None,
            data: Some(UserData{
                name: user[0].name.clone(),
                admin: user[0].admin.clone(),
            })
        }).unwrap())))
    }
}

