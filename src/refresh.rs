use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;
use super::Session;
use super::serde_json;

use std::io::Read;


pub struct Refresh{
    session: Session,
}

#[derive(Serialize)]
pub struct RefreshResponse<'a>{
    succesfull: bool,
    error: Option<&'a str>,
}

#[derive(Deserialize)]
pub struct RefreshRequest{
    session_id: u64,
}

unsafe impl Send for Refresh{}
unsafe impl Sync for Refresh{}

impl Refresh{
    pub fn new(session: Session) -> Self{
        Refresh{
            session: session,
        }
    }
}

impl Handler for Refresh{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("like request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("like request body: {}",body);
        let like_req: RefreshRequest = match serde_json::from_str(&body){
            Ok(x) => x,
            Err(_) => {
                return Ok(Response::with((status::Ok,serde_json::to_string_pretty(&RefreshResponse{
                    succesfull: false,
                    error: Some("Bad request"),
                }).unwrap())));
            }
        };

        match self.session.get(like_req.session_id){
            Some(_) => {
                Ok(Response::with((status::Ok,serde_json::to_string_pretty(&RefreshResponse{
                    succesfull: true,
                    error: None,
                }).unwrap())))
            }
            None => {
                Ok(Response::with((status::Ok,serde_json::to_string_pretty(&RefreshResponse{
                    succesfull: false,
                    error: Some("Not logged in."),
                }).unwrap())))
            },
        }
    }
}
