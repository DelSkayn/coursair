use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::db::schema::courses::dsl::{courses};
use super::db::models::Course;
use super::db::models::NewCourses;

use std::io::Read;

use super::Session;
use super::serde_json;


use std::sync::Arc;
use std::sync::Mutex;



pub struct NewCourse{
    connection: Arc<Mutex<PgConnection>>,
    session: Session,
}

#[derive(Serialize)]
pub struct NewCourseResponse{
    succesfull: bool,
    course_id: Option<i32>,
}

unsafe impl Send for NewCourse{}
unsafe impl Sync for NewCourse{}

impl NewCourse{
    pub fn new(connection: Arc<Mutex<PgConnection>>,session: Session) -> Self{
        NewCourse{
            connection: connection,
            session: session,
        }
    }
}

impl Handler for NewCourse{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("login request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("login request body: {}",body);

        let new_course = NewCourses{
            name: "new course".to_string(),
            language: String::new(),
            periode: String::new(),
            year: String::new(),
            docenten: String::new(),
            ec: String::new(),
            niveau: String::new(),
            summary: Some(String::new()),
            discription: Some(String::new()),
        };

        let inserted_course: Vec<_> = itry!(diesel::insert(&new_course)
            .into(courses)
            .get_results::<Course>(&*self.connection.lock().unwrap()));

        Ok(Response::with((status::Ok,serde_json::to_string_pretty(&NewCourseResponse{
            succesfull: true,
            course_id: Some(inserted_course[0].id),
        }).unwrap())))
    }
}
