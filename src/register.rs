use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;
use super::db::schema::users as db_users;
use super::db::schema::users::dsl::*;
use super::db::models::User;
use super::db::models::NewUser;

use super::serde_json;
use super::rand::{self, ThreadRng, Rng};

use std::sync::Arc;
use std::sync::Mutex;
use std::hash::{Hasher, Hash};
use std::collections::hash_map::DefaultHasher;

use std::io::Read;

pub struct Register {
    connection: Arc<Mutex<PgConnection>>,
    random: ThreadRng,
}

#[derive(Serialize)]
struct RegisterResponse<'a> {
    succesfull: bool,
    error: Option<&'a str>,
}

#[derive(Deserialize)]
struct RegisterRequest {
    username: String,
    password: String,
    mail: String,
}

unsafe impl Send for Register {}
unsafe impl Sync for Register {}

impl Register {
    pub fn new(connection: Arc<Mutex<PgConnection>>) -> Self {
        Register {
            connection: connection,
            random: rand::thread_rng(),
        }
    }
}

impl Handler for Register {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        debug!("Register!");
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("Reg body: {}",body);

        // parse the body of the request.
        let reg_req: RegisterRequest = match serde_json::from_str(&body) {
            Ok(x) => x,
            Err(_) => {
                debug!("Register failed invalid body!");
                return Ok(Response::with((status::Ok,
                                          serde_json::to_string_pretty(&RegisterResponse {
                        succesfull: false,
                        error: Some("invalid body"),
                    })
                    .unwrap())));
            }
        };
        let does_user_exist = itry!(users.filter(name.eq(reg_req.username.clone()))
            .load::<User>(&*self.connection.lock().unwrap()));

        if does_user_exist.len() > 0 {
            debug!("Register failed user already exits!");
            return Ok(Response::with((status::Ok,
                                      serde_json::to_string_pretty(&RegisterResponse {
                    succesfull: false,
                    error: Some("username already exists"),
                })
                .unwrap())));
        }

        // salt and shit.
        let new_salt = self.random.clone().next_u64() as i64;
        let mut hasher = DefaultHasher::new();
        reg_req.password.hash(&mut hasher);
        hasher.write_i64(new_salt);
        let password_hash = hasher.finish();
        let new_user = NewUser {
            name: &reg_req.username,
            password: password_hash as i64,
            salt: new_salt,
            admin: false,
        };


        itry!(diesel::insert(&new_user)
            .into(db_users::table)
            .execute(&*self.connection.lock().unwrap())
            .map_err(|e| {
                error!("Error inserting user in data base!");
                e
            }));

        info!("register succesfull!");

        Ok(Response::with((status::Ok,
                           serde_json::to_string_pretty(&RegisterResponse {
                succesfull: true,
                error: None,
            })
            .unwrap())))
    }
}
