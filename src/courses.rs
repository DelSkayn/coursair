use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::diesel::pg::expression::dsl::any;
use super::diesel::expression::sql_literal::SqlLiteral;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;


use super::db::schema::courses::dsl::{courses,year,niveau,language,ec,periode,name};
use super::db::schema::has_tags::dsl::{course_id,tag_id,has_tags};
use super::db::schema::tags::dsl::{tags,id};
use super::db::models::Course;
use super::db::models::Tag;

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;
use std::io::Read;

pub struct Courses {
    connection: Arc<Mutex<PgConnection>>,
}

#[derive(Deserialize)]
pub struct Filter{
    years: Vec<String>,
    languages: Vec<String>,
    periodes: Vec<String>,
    ecs: Vec<String>,
    niveaus: Vec<String>,
}

#[derive(Deserialize)]
pub struct CourseRequest{
    filter: Filter,
    search: Option<String>,
}

#[derive(Serialize)]
pub struct CourseResult {
    pub id: i32,
    pub name: String,
    pub language: String,
    pub periode: String,
    pub year: String,
    pub docenten: String,
    pub ec: String, 
    pub niveau: String,
    pub tags: Vec<Tag>,
}

impl Courses {
    pub fn new(connection: Arc<Mutex<PgConnection>>) -> Self {
        Courses { connection: connection }
    }
}

impl Handler for Courses {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        debug!("course request: {:#?}",req);
        let mut body = String::new();
        itry!(req.body.read_to_string(&mut body));
        debug!("course body: {}",body);
        let course_req: CourseRequest = itry!(serde_json::from_str(&body));

        let con = self.connection.lock().unwrap();
        let loaded_refs: Vec<(i32,i32)> = itry!(courses.inner_join(has_tags)
            .select((course_id,tag_id))
            .load(&*con));

        let mut needed_tags: Vec<_> = loaded_refs.iter().map(|e| e.1).collect();
        needed_tags.dedup();
        let loaded_tag: Vec<Tag> = itry!(tags.filter(id.eq_any(&needed_tags)).load(&*con));

        let mut load = courses.into_boxed();
        if course_req.filter.years.len() > 0{
            load = load.filter(year.eq(any(&course_req.filter.years)));
        }
        if course_req.filter.languages.len() > 0{
            load = load.filter(language.eq(any(&course_req.filter.languages)));
        }
        if course_req.filter.ecs.len() > 0{
            load = load.filter(ec.eq(any(&course_req.filter.ecs)));
        }
        if course_req.filter.periodes.len() > 0{
            load = load.filter(periode.eq(any(&course_req.filter.periodes)));
        }
        if course_req.filter.niveaus.len() > 0{
            load = load.filter(niveau.eq(any(&course_req.filter.niveaus)));
        }
        if let Some(x) = course_req.search.as_ref() {
            if x.len() > 0{
                let mut prepared = x.replace(" ","% %");
                prepared.push('%');
                prepared.insert(0,'%');
                load = load.filter(SqlLiteral::new( format!(" name ILIKE \'{}\'",prepared)));
                println!("Prepared! {} :",prepared);
            }
        }


        let mut loaded_courses: Vec<Course> = itry!(load.load(&*con));

        let mut res = Vec::with_capacity(loaded_courses.len());
        for course in loaded_courses.drain(..){
            let mut new_course = CourseResult{
                id: course.id,
                name: course.name,
                language: course.language,
                periode: course.periode,
                year: course.year,
                docenten: course.docenten,
                ec: course.ec,
                niveau: course.niveau,
                tags: Vec::new(),
            };
            for r in loaded_refs.iter(){
                if r.0 == course.id{
                    for tag in loaded_tag.iter(){
                        if tag.id == r.1{
                            new_course.tags.push(tag.clone())
                        }
                    }
                }
            }
            res.push(new_course);
        }
        Ok(Response::with((status::Ok, serde_json::to_string_pretty(&res).unwrap())))
    }
}
