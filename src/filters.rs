use super::diesel::pg::PgConnection;
use super::diesel::prelude::*;
use super::iron::prelude::*;
use super::iron::Handler;
use super::iron::status;

use super::db::schema::courses::dsl::courses;
use super::db::schema::courses::dsl::{year,language,periode,ec,niveau};

use super::serde_json;

use std::sync::Arc;
use std::sync::Mutex;


pub struct Filter{
    connection: Arc<Mutex<PgConnection>>,
}

#[derive(Serialize)]
pub struct FilterResponse{
    pub years: Vec<String>,
    pub languages: Vec<String>,
    pub periodes: Vec<String>,
    pub ecs: Vec<String>,
    pub niveaus: Vec<String>,
}


unsafe impl Send for Filter{}
unsafe impl Sync for Filter{}

impl Filter{
    pub fn new(connection: Arc<Mutex<PgConnection>>) -> Self{
        Filter{
            connection: connection,
        }
    }
}

impl Handler for Filter{
    fn handle(&self, req: &mut Request) -> IronResult<Response>{
        debug!("is liked request: {:#?}",req);

        let con = self.connection.lock().unwrap();
        let years: Vec<String>     = itry!(courses.select(year).distinct().load(&*con));
        let niveaus: Vec<String>   = itry!(courses.select(niveau).distinct().load(&*con));
        let languages: Vec<String> = itry!(courses.select(language).distinct().load(&*con));
        let ec_s: Vec<String>      = itry!(courses.select(ec).distinct().load(&*con));
        let periodes: Vec<String>  = itry!(courses.select(periode).distinct().load(&*con));

        Ok(Response::with((status::Ok,itry!(serde_json::to_string_pretty(&FilterResponse{
            years: years,
            languages: languages,
            periodes: periodes,
            ecs: ec_s,
            niveaus: niveaus,
        })))))
    }
}
