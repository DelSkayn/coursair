module IndexUpdate exposing (Msg(..), update, load_courses)

import IndexModel exposing (Model, CourseData,TagData)
import Common.Update exposing (Msg(..),encode_filters,FilterEventType(..))
import Platform.Cmd exposing (map)
import Task exposing (perform)
import Http exposing (get)
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (decode, required, optional)
import Json.Encode


type Msg
    = Common Common.Update.Msg
    | RequestCourses
    | CourseSucceed (List CourseData)
    | RequestFail Http.Error


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
        case msg of
            Common com_msg ->
                case com_msg of
                    Common.Update.FilterEvent e ->
                        let 
                            this =
                                model.common

                            ( common_model, new_msg ) =
                                Common.Update.update com_msg this

                            new_model = {model | common = common_model}
                            msg = case e of
                                Add _ _ -> 
                                    [ load_courses new_model ]
                                Remove _ _ -> 
                                    [ load_courses new_model ]
                                _ ->
                                    []
                        in
                            (new_model, Cmd.batch (Cmd.map Common new_msg :: msg))

                    Common.Update.SearchInput _ ->
                        let 
                            this =
                                model.common

                            ( common_model, new_msg ) =
                                Common.Update.update com_msg this

                            new_model = {model | common = common_model}
                            msg = [load_courses new_model]
                        in
                            (new_model, Cmd.batch (Cmd.map Common new_msg :: msg))


                    other ->
                        let
                            this =
                                model.common

                            ( common_model, new_msg ) =
                                Common.Update.update com_msg this
                        in
                            ( { model | common = common_model }, Cmd.map Common new_msg )
            RequestCourses ->
                ( model, load_courses model)

            CourseSucceed a ->
                ( { model | courses = a }, Cmd.none )

            RequestFail e ->
                ( { model | could_not_retrieve = True, error = Just e }, Cmd.none )



encode_course_request : Model -> String
encode_course_request model = 
    let 
        filter = model.common.current_filters
    in
    Json.Encode.encode 0
        (Json.Encode.object
            [ ( "filter", Json.Encode.object 
                [ ( "years", Json.Encode.list (List.map Json.Encode.string filter.years))
                , ( "languages", Json.Encode.list (List.map Json.Encode.string filter.languages))
                , ( "periodes", Json.Encode.list (List.map Json.Encode.string filter.periodes))
                , ( "ecs", Json.Encode.list (List.map Json.Encode.string filter.ecs))
                , ( "niveaus", Json.Encode.list (List.map Json.Encode.string filter.niveaus))
                ])
            , ( "search", Json.Encode.string model.common.search)])
            


load_courses : Model -> Cmd Msg
load_courses model =
    let
        url =
            "data/courses"
        body = encode_course_request model
        request = Http.post url (Http.stringBody "application/json" body) course
        which a = case a of
            Ok(x) -> CourseSucceed x
            Err(x) -> RequestFail x
    in
        Http.send which request 


course : Decoder (List CourseData)
course = 
    list 
        (decode CourseData
            |> required "id" int
            |> required "name" string
            |> required "language" string
            |> required "periode" string
            |> required "year" string
            |> required "docenten" string
            |> required "ec" string
            |> required "niveau" string
            |> required "tags" (list
                (decode TagData
                    |> required "id" int
                    |> required "name" string)))

