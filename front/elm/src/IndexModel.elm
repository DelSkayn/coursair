module IndexModel exposing (CourseData,Model,initial,TagData)
import Common.Model
import Http

type alias CourseData =
    { id : Int
    , name : String
    , language : String
    , periode : String
    , year : String
    , docenten : String
    , ec : String
    , niveau : String
    , tags : (List TagData)}

type alias TagData =
    { id : Int
    , name : String}

type alias Model = 
    { common : Common.Model.Model
    , could_not_retrieve : Bool
    , error : Maybe Http.Error
    , courses : List CourseData }

initial : Model
initial = 
    { common = (Common.Model.initial True)
    , could_not_retrieve = False
    , error = Nothing
    , courses = []}
