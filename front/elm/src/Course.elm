import Html
import Common.Update exposing (check_session,filters)
import CourseModel exposing (Model,initial)
import CourseUpdate exposing (update,Msg(..))
import CourseView exposing (view)
import Platform.Cmd

subscriptions : Model -> Sub msg
subscriptions model =
    Sub.none


main = 
    let 
        init a = (initial a, Cmd.batch 
            [ Cmd.map Common check_session
            , Cmd.map Common filters ])
    in
    Html.programWithFlags
        { init = init 
        , update = update
        , view = view
        , subscriptions = subscriptions }
