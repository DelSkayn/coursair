module AddCourseModel exposing(..)

type alias TagData =
    { id : Int
    , name : String}

type alias CourseData =
    { name : String
    , language : String
    , periode : String
    , year : String
    , docenten : String
    , ec : String
    , niveau : String
    , discription : String
    , summary : String
    , tags : List TagData}

type alias Model =
    { current_course: CourseData
    , course_id: Int}

initial : Model
initial = 
    {current_course = 
        { name = ""
        , language = ""
        , periode = ""
        , year = ""
        , docenten = ""
        , ec = ""
        , niveau = ""
        , discription = ""
        , summary = ""
        , tags = []}
    , course_id = -1}

