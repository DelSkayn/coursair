module AddCourseUpdate exposing (..)
import AddCourseModel exposing (..)
import Http
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (decode, required, optional)
import Json.Encode
import String

type Msg 
    = Name String
    | Language String
    | Periode String
    | Year String
    | Docenten String
    | Ec String
    | Niveau String
    | Discription String
    | Summary String

    | Select String
    | Submit

    | CourseSucceed CourseRespone
    | RequestFail Http.Error
    | NewCourse
    | NewCourseSucceed NewCourseResponse

    | Noop

type alias CourseRequest =
    { course_id: Int }

type alias CourseRespone =
    { succesfull : Bool
    , course : Maybe CourseData }

type alias NewCourseResponse = 
    { succesfull : Bool
    , course_id : Maybe Int }


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
        case msg of
            CourseSucceed x ->
                case x.course of 
                    Just y ->
                        ({model | current_course = y},Cmd.none)
                    Nothing ->
                        (model,Cmd.none)
            NewCourseSucceed x ->
                case x.course_id of 
                    Just y ->
                        ({model | course_id = y},load_course y)
                    Nothing ->
                        (model,Cmd.none)
            NewCourse ->
                (model,new_course)
            Noop ->
                (model,Cmd.none)
            RequestFail _ ->
                (model,Cmd.none)
            Submit ->
                (model,submit_course model)
            Select x ->
                case String.toInt x of 
                    Ok y -> 
                        ({model | course_id = y},load_course y)
                    Err _ ->
                        (model,Cmd.none)
            Name x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | name = x}},Cmd.none)
            Language x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | language = x}},Cmd.none)
            Periode x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | periode = x}},Cmd.none)
            Year x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | year = x}},Cmd.none)
            Docenten x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | docenten = x}},Cmd.none)
            Ec x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | ec = x}},Cmd.none)
            Niveau x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | niveau = x}},Cmd.none)
            Discription x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | discription = x}},Cmd.none)
            Summary x ->
                let 
                    course = model.current_course
                in 
                    ({model | current_course = {course | summary = x}},Cmd.none)


load_request : Decoder CourseRespone
load_request = 
    Json.Decode.map2 CourseRespone
        (at ["succesfull"] bool)
        (maybe(at ["course"] course)) 

course : Decoder CourseData
course = 
    decode CourseData
        |> required "name" string
        |> required "language" string
        |> required "periode" string
        |> required "year" string
        |> required "docenten" string
        |> required "ec" string
        |> required "niveau" string
        |> required "discription" string
        |> required "summary" string
        |> required "tags" (list
            (decode TagData
                |> required "id" int
                |> required "name" string))

submit_course : Model -> Cmd Msg
submit_course model = 
    let 
        url = "data/alter"
        body = course_to_json model
        request = Http.post url (Http.stringBody "application/json" body) load_request
    in
        Http.send (always Noop) request

load_course : Int -> Cmd Msg
load_course model = 
    let 
        url = "data/single"
        body = course_id_to_json model
        request = Http.post url (Http.stringBody "application/json" body) load_request
        which a = case a of
            Ok(x) -> CourseSucceed x 
            Err(x) -> RequestFail x
    in
        Http.send which request

new_course : Cmd Msg
new_course = 
    let 
        url = "data/new_course"
        request = Http.get url decode_new_course
        which a = case a of
            Ok(x) -> NewCourseSucceed x 
            Err(x) -> RequestFail x
    in
        Http.send which request

decode_new_course : Decoder NewCourseResponse
decode_new_course = 
    Json.Decode.map2 NewCourseResponse
        (at ["succesfull"] bool)
        (maybe(at ["course_id"] int)) 

course_id_to_json : Int -> String
course_id_to_json r =
    Json.Encode.encode 0 (Json.Encode.object
                [ ("course_id", Json.Encode.int r ) ])

course_to_json : Model -> String
course_to_json r =
    Json.Encode.encode 0 (Json.Encode.object
                [ ("id", Json.Encode.int r.course_id ) 
                , ("name", Json.Encode.string r.current_course.name)
                , ("language", Json.Encode.string r.current_course.language)
                , ("periode", Json.Encode.string r.current_course.periode)
                , ("year", Json.Encode.string r.current_course.year)
                , ("docenten", Json.Encode.string r.current_course.docenten)
                , ("ec", Json.Encode.string r.current_course.ec)
                , ("niveau", Json.Encode.string r.current_course.niveau)
                , ("discription", Json.Encode.string r.current_course.discription)
                , ("summary", Json.Encode.string r.current_course.summary) ])

