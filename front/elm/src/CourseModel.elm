module CourseModel exposing (CourseData, Model, initial,Show(..),TagData,OtherCourseData,Comment)
import Common.Model exposing (Model)
import Common.Update exposing (check_session)
import Http
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (decode, required, optional)
import Debug

type Show 
    = Detail
    | More
    | Other
    | Comments


type alias CourseData =
    { id : Int
    , name : String
    , language : String
    , periode : String
    , year : String
    , docenten : String
    , ec : String
    , niveau : String
    , discription : String
    , summary : String
    , tags : List TagData
    , other_courses : List OtherCourseData}

type alias OtherCourseData =
    { id : Int
    , name : String
    , language : String
    , periode : String
    , year : String
    , docenten : String
    , ec : String
    , niveau : String}

type alias TagData =
    { id : Int
    , name : String}

type alias Comment =
    { username : String
    , content : String }


type alias Model = 
    { common : Common.Model.Model
    , could_not_retrieve : Bool
    , comment : String
    , login_dialog: Bool
    , user_liked : Bool
    , show : Show
    , error : Maybe Http.Error
    , course : CourseData 
    , comments : List Comment }

initial : String -> Model
initial new_course = 
    let 
        val = case decodeString course new_course of
            Ok val ->
                val
            Err e ->
                Debug.log e
                { id = -1
                , name = "Error"
                , language = "Error"
                , periode = "Error"
                , year = "Error"
                , docenten = "Error"
                , ec = "Error"
                , niveau = "Error"
                , discription = "Error" 
                , summary = "Error"
                , tags = []
                , other_courses = []}
    in
        { common = (Common.Model.initial False)
        , could_not_retrieve = False
        , comment = ""
        , login_dialog = False
        , show = Detail
        , user_liked = False
        , error = Nothing
            , comments = []
        , course = val}

course : Decoder CourseData
course = 
    decode CourseData
        |> required "id" int
        |> required "name" string
        |> required "language" string
        |> required "periode" string
        |> required "year" string
        |> required "docenten" string
        |> required "ec" string
        |> required "niveau" string
        |> required "discription" string
        |> required "summary" string
        |> required "tags" (list
            (decode TagData
                |> required "id" int
                |> required "name" string))
        |> required "other_courses" (list
            (decode OtherCourseData
                |> required "id" int
                |> required "name" string
                |> required "language" string
                |> required "periode" string
                |> required "year" string
                |> required "docenten" string
                |> required "ec" string
                |> required "niveau" string))
