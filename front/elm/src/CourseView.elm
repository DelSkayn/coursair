module CourseView exposing (view)
import CourseUpdate exposing(Msg(..))
import CourseModel exposing (Model,CourseData,Show(..),TagData,OtherCourseData,Comment)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput,onClick, onSubmit)
import Common.View as CommonView
import Common.Update
import Common.Sidebar.Part as Sidebar
import Markdown


view : Model -> Html Msg
view model = 
    let 
        this = model.common
    in
        CommonView.view this Common [ content model ]

content : Model -> Html Msg
content model = 
    let 
        page_content = case model.show of
            Detail -> 
                div [class "detail-info"] 
                    [ table [ class "detail-info" ] 
                       [ tr [ class "detail-info-language" ] 
                            [ td [ class "detail-info-type" ] [ text "language" ]
                            , td [ class "detail-info-data" ] [text model.course.language ]]
                       , tr [ class "detail-info-language" ] 
                            [ td [ class "detail-info-type" ] [ text "Year" ]
                            , td [ class "detail-info-data" ] [ text model.course.year ]]
                       , tr [ class "detail-info-language" ] 
                            [ td [ class "detail-info-type" ] [ text "period" ]
                            , td [ class "detail-info-data" ] [ text model.course.periode ]]
                       , tr [ class "detail-info-language" ] 
                            [ td [ class "detail-info-type" ] [ text "teachers" ]
                            , td [ class "detail-info-data" ] [ text model.course.docenten ]]
                       , tr [ class "detail-info-language" ] 
                            [ td [ class "detail-info-type" ] [ text "EC" ]
                            , td [ class "detail-info-data" ] [ text model.course.ec ]]
                       , tr [ class "detail-info-language" ] 
                            [ td [ class "detail-info-type" ] [ text "Level" ]
                            , td [ class "detail-info-data" ] [ text model.course.niveau ]]]
                    , Markdown.toHtml [class "detail-discription" ] model.course.discription ]
            More -> 
                recommendations model
            Other ->
                recommendations model
            Comments ->
                div [ class "comment-content"]
                    [ div [ class "comment-form"]
                           [ textarea [ placeholder "Comment", onInput CommentInput, onClick CommentClick,value model.comment ] [] 
                           , div [ class "comment-submit", onClick RequestNewComment ] [ i [class "material-icons"] [ text "send" ]]]
                    , ul [ class "comments" ] (if List.isEmpty model.comments then
                            [text "There are no comments for this course, but you can add one!"] 
                        else 
                            (comments_list model.comments))]

        like_icon = 
            case model.common.id of 
                Just _ -> case model.user_liked of
                    True -> i [ class "material-icons" ]
                              [ text "favorite"]
                    False -> i [ class "material-icons" ]
                              [ text "favorite_border"]
                Nothing ->
                    i [ class "material-icons" ]
                          [ text "favorite_border"]
    in 
        div [ class "detail" ] 
            [ login_dialog model
            , div [ class "detail-header" ]
                  [ text model.course.name ]
            , div [ class "detail-subheader" ]
                  [ li  [ class "tag-list detail-tag-list" ] (tag_list model.course.tags)
                  , div [ class "button like-button", onClick Like ] [ like_icon ]]
            , div [ class "detail-options" ]
                  [ div [class (case model.show of
                                    Detail -> "button button-active"
                                    _ -> "button")
                        , onClick (Change Detail) ] [text "Info" ]
                  , div [class (case model.show of
                                    More -> "button button-active"
                                    _ -> "button")
                        , onClick (Change More) ] [ text "More" ]
                  , div [class (case model.show of
                                    Comments -> "button button-active"
                                    _ -> "button")
                        , onClick (Change Comments) ] [ text "What other thought!" ]]
            , div [ class "detail-content-wrap" ]
                  [ page_content ]]

login_dialog : Model -> Html Msg
login_dialog model =
    if model.login_dialog then
        div [ class "login-dialog" ]
            [ div [ class "login-dialog-header" ]
                  [ text "Please login" 
                  , div [ class "login-dialog-close", onClick CloseLoginDialog ]
                        [ i [class "material-icons"] [text "clear"] ]]
            , div [ class "login-dialog-content" ]
                  [ text "In order to favorite a course or comment on a course you need to be logged in." ]
            , div [ class "login-dialog-button button" , onClick LoginDialogLogin ]
                  [ text "Login" ]]
    else
        div [style [("Display","None")]][]


other_content_data : OtherCourseData -> Html msg
other_content_data course = 
    let
        url = "/course?id=" ++ (toString course.id)
    in
    div [ class "course-panel" ] 
        [ div [ class "course-summary" ]
              [ a [ href url ] 
                  [ div [ class "course-header" ]
                        [ text course.name ]]
                  , table [ class "detail-info" ] 
                        [ tr [ class "detail-info-language" ] 
                             [ td [ class "detail-info-type" ] [ text "language" ]
                             , td [ class "detail-info-data" ] [text course.language ]]
                        , tr [ class "detail-info-language" ] 
                             [ td [ class "detail-info-type" ] [ text "Year" ]
                             , td [ class "detail-info-data" ] [ text course.year ]]
                        , tr [ class "detail-info-language" ] 
                             [ td [ class "detail-info-type" ] [ text "periode" ]
                             , td [ class "detail-info-data" ] [ text course.periode ]]
                        , tr [ class "detail-info-language" ] 
                             [ td [ class "detail-info-type" ] [ text "Docenten" ]
                             , td [ class "detail-info-data" ] [ text course.docenten ]]
                        , tr [ class "detail-info-language" ] 
                             [ td [ class "detail-info-type" ] [ text "EC" ]
                             , td [ class "detail-info-data" ] [ text course.ec ]]
                        , tr [ class "detail-info-language" ] 
                             [ td [ class "detail-info-type" ] [ text "Niveau" ]
                             , td [ class "detail-info-data" ] [ text course.niveau ]]]]]

tag_list : (List TagData) -> List (Html msg )
tag_list tags = 
    let 
        res = case List.tail tags of
            Nothing -> []
            Just x -> x
    in
        case List.head tags of 
            Just a -> 
                li [class "tag-list-item"] [ text a.name ]
                    :: (tag_list res)
            Nothing -> []

comments_list : (List Comment) -> List (Html Msg)
comments_list comments = 
    let 
        res = case List.tail comments of
            Nothing -> []
            Just x -> x
    in
        case List.head comments of 
            Just a ->
                (comment_html a)
                    :: (comments_list res)
            Nothing -> []

comment_html : Comment -> Html Msg
comment_html comment =
    li [ class "comment" ]
        [ div [ class "username"  ]
              [ text comment.username ]
        , Markdown.toHtml [ class "comment-content" ]
              comment.content ]
              
recommendations : Model -> Html msg
recommendations model =
    if List.isEmpty model.course.other_courses then
        div [class "recommendations" ] 
            [ text "There are no recommendations for this course."]
    else
        div [class "recommendations" ] 
            [ ul [ class "recommendation-list"] (recommendation_list model.course.other_courses)]

recommendation_list : List OtherCourseData -> List (Html msg)
recommendation_list l = 
    let 
        res = case List.tail l of
            Nothing -> []
            Just x -> x
    in
        case List.head l of 
            Just a -> 
                li [class "tag-list-item"] [ other_content_data a ]
                    :: (recommendation_list res)
            Nothing -> []

