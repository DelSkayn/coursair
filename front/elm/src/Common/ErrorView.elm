module Common.ErrorView exposing(error_view)
import Html exposing (..)
import Html.Attributes exposing (..)
import Http exposing (Error(..))
import Common.Model exposing (Model,Error(..))
import Common.Update exposing (Msg)
import LocalStorage exposing(Error(..))

error_view : Model -> Html Msg
error_view model = 
    let error_message =
        case model.error of 
            Nothing -> "I dont even know what went wrong!"
            Just x -> case x of
                HttpError y -> case y of 
                    BadUrl _ ->
                        "It seems i made a mistake and asked for the wrong thing."
                    Timeout -> 
                        "It seems the server is down! We will get our best men on this problem."
                    NetworkError ->
                        "Well it seems the network is not doing what it should."
                    BadStatus _ -> 
                        "It seems the server got confused!"
                    BadPayload _ _ ->
                        "The server does not seem to want to talk. :("
                LocalStorageError y -> case y of 
                    UnexpectedPayload _ ->
                        "It seems i retriefed some unusual data from the local storage!"
                    NoStorage ->
                        "Your browser unfortunatly does not support localStorage. I really kinda need that..."
    in 
        div [ id "wrap" ]
            [ div [ id "error-header"]
                  [ text "Oops!"]
            , div [ id "error-subtitle"]
                  [ text "It seems something went wrong." ]
            , div [ id "error-message" ]
                  [ text error_message ]]
