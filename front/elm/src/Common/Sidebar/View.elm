module Common.Sidebar.View exposing (sidebar,sidebar_content,icon)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput,onClick, onSubmit)
import Common.Sidebar.Part exposing (Model,OpenMenu,Msg(..))

(=>) = (,)

sidebar : Model -> (Msg -> a) -> List(Html Msg) -> List(Html a) -> Html a 
sidebar model trans icons content = 
    let display =
        if model.active then
            "block"
        else
            "none"
    in
        div [navbar model.active, id "side-bar"]
            [ div [style ["display" => display], class "side-bar-content"] content
            , Html.map trans 
            ( div [class "side-bar-menu"]
                  [  a [ href "/" ] 
                       [div [ class "logo" ] 
                            [ img [src "res/Logo.png"] []]]
                  , div [] icons])]

sidebar_content: Model -> OpenMenu -> List(Html a) -> Html a
sidebar_content model message inner = 
    let display = 
        if model.which == message then
            "block"
        else
            "none"
    in
        div[ style ["display" => display] ]  inner

icon : Model -> String -> String -> Int -> OpenMenu -> Html Msg
icon model name icon_name size message =
    let activated =
            model.which == message
    in 
        div [classList ["menu-icon" => True,"active" => activated], onClick (Activate message)] 
            [ i [class ("material-icons md-" ++ (toString size))] 
                [ text icon_name]
            , div [class "icon-text"]
                  [ text name ]]

navbar : Bool -> Attribute msg
navbar active = 
    let w = 
        if active then
            "370px"
        else
            "70px"
    in
        style [ "width" => w ]
