module Common.Sidebar.Part exposing(..)
import Platform.Cmd exposing (map)

type OpenMenu 
    = None
    | Login
    | Menu
    | Search
    | Filter

type alias Model =
    { active : Bool
    , which : OpenMenu}


type Msg 
    = Activate OpenMenu
    | Deactivate

update : Msg -> Model -> Model
update msg model = 
    case msg of 
        Activate which ->
            {model | active = True
                    , which = which}
        Deactivate ->
            {model | active = False
                    , which = None}



initial : Model
initial = 
    { active = False
    , which = None }
