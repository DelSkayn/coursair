module Common.View exposing (view)
import Common.Sidebar.View exposing(sidebar,icon,sidebar_content)
import Common.Sidebar.Part as SidePart exposing(OpenMenu(..))
import Common.Model as Model exposing (..)
import Css
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput,onClick, onSubmit)
import Common.Update exposing (..)
import Common.ErrorView as ErrorView
import Regex

(=>) = (,)

view : Model -> (Msg -> a) -> List (Html a) -> Html a 
view model trans content = 
    let 
        margin = if model.sidebar.active then
                "370px"
            else
                "70px"
    in
    case model.error of
        Nothing ->
            case model.id of 
                Nothing ->
                    div [id "wrap"]
                        [Html.map trans (sidebar model.sidebar SideBar
                                        [ icon model.sidebar "Login" "face" 36 Login
                                        , icon model.sidebar "Search" "search" 36 Search 
                                        , icon model.sidebar "Filter" "filter_list" 36 Filter ]

                                        [ sidebar_content model.sidebar Menu [ text "menu" ]
                                        , log_bar_content model 
                                        , search_bar_content model
                                        , filter_bar_content model ]
                                    )
                        , div [ id "content", onClick (trans (SideBar SidePart.Deactivate)),style [("margin-left", margin)]]
                               content ]
                Just _ ->
                    div [id "wrap"]
                        [Html.map trans (sidebar model.sidebar SideBar
                                        [ icon model.sidebar "User" "face" 36 Login
                                        , icon model.sidebar "Search" "search" 36 Search 
                                        , icon model.sidebar "Filter" "filter_list" 36 Filter ]

                                        [ log_bar_content model 
                                        , search_bar_content model
                                        , filter_bar_content model ]
                                    )
                        , div [ id "content", onClick (trans (SideBar SidePart.Deactivate)),style [("margin-left", margin)]]
                               content ]

        Just _ ->
            Html.map trans (ErrorView.error_view model)


log_bar_content : Model -> Html Msg
log_bar_content model = 
    let 
        error_html = error model.login
    in
        case model.id of
            Nothing ->
                sidebar_content model.sidebar Login 
                    [ div [class "login-content"] 
                          [ div [class "login-input"]
                                [Html.form [onSubmit RequestLogin] 
                                    [ div [ class "sidebar-header" ] [ text "Please login." ]
                                    , input [name "username", placeholder "Username", onInput UsernameInput, value model.login.username] []
                                    , input [name "password",type_ "password", placeholder "Password", onInput PasswordInput, value model.login.password] []
                                    , br [] []
                                    , input [class "submit", type_ "submit", value "login!"] [text "login" ] ]
                                ,div [ class "devider" ] [ text "or" ]
                                ,Html.form [onSubmit RequestRegister ] 
                                    [ div [ class "register-header" ] [ text "Register a new acount!" ]
                                    , error_html
                                    , input [name "username", placeholder "Username", onInput UsernameInput, value model.login.username] []
                                    , input [name "mail", placeholder "Email Address", onInput MailInput, value model.login.mail] []
                                    , input [name "password",type_ "password", placeholder "Password", onInput PasswordInput, value model.login.password] []
                                    , input [name "password-again",type_ "password", placeholder "Password again", onInput PasswordAgainInput, value model.login.password_again] [] 
                                    , input [class "submit", type_ "submit", value "Register!"] [text "login"] ]]]]
            Just _ -> 
                sidebar_content model.sidebar Login 
                    [ div [class "user-content" ] 
                          [ div [ class "button", onClick Logout ]
                                [ text "Logout"]]]

search_bar_content : Model -> Html Msg
search_bar_content model =
    sidebar_content model.sidebar Search
        [ div [ class "search-content" ]
              [ div [ class "sidebar-header" ] [ text "Search a course." ]
              , input [ placeholder "Search", onInput SearchInput, value model.search, class "search-input" ] []]]

filter_bar_content : Model -> Html Msg
filter_bar_content model =
    let 
        cur = model.current_filters
        opt = model.filter_options
        open = model.filter_view
    in
    sidebar_content model.sidebar Filter
        [ div [ class "filter-content" ]
              [ div [class "sidebar-header"]
                    [text  "Select a filter." ]
              , div [class "filter-header"] [ text "Year" ]
              , filter_menu opt.years cur.years open.years Year
              , div [class "filter-button", onClick (FilterEvent (Open Year))] [ i [class "material-icons"] [text "arrow_drop_down"]]
              , div [class "filter-header"] [ text "Language" ]
              , filter_menu opt.languages cur.languages open.languages Language
              , div [class "filter-button", onClick (FilterEvent (Open Language))] [ i [class "material-icons"] [text "arrow_drop_down"]]
              , div [class "filter-header"] [ text "Periodes" ]
              , filter_menu opt.periodes cur.periodes open.periodes Period
              , div [class "filter-button", onClick (FilterEvent (Open Period))] [ i [class "material-icons"] [text "arrow_drop_down"]]
              , div [class "filter-header"] [ text "Ec" ]
              , filter_menu opt.ecs cur.ecs open.ecs Ec
              , div [class "filter-button", onClick (FilterEvent (Open Ec))] [ i [class "material-icons"] [text "arrow_drop_down"]]
              , div [class "filter-header"] [ text "Niveaus" ]
              , filter_menu opt.niveaus cur.niveaus open.niveaus Niveau 
              , div [class "filter-button", onClick (FilterEvent (Open Niveau))] [ i [class "material-icons"] [text "arrow_drop_down"]]]]

filter_menu : List String -> List String -> Bool -> FilterType -> Html Msg
filter_menu options current open ty =
    let 
        drop_down = case open of
            False -> div [class "filter-dropdown"] []
            True -> 
                let 
                    content = case filter_dropdown ty 0 options of
                        [] -> [div [ class "empty-dropdown" ] [text "No filters left"]]
                        x -> x
                in 
                    div [ class "filter-dropdown"] 
                        [ div [ class "filter-dropdown-content"] content]
    in
        div [ class "filter-menu" ]
            [ li [ class "filter-list" ] (filter_list ty 0 current) 
            , drop_down ]

filter_list : FilterType -> Int -> List String -> List (Html Msg) 
filter_list ty num l = 
    let 
        res = case List.tail l of
            Nothing -> []
            Just x -> x
    in
        case List.head l of 
            Just a -> 
                li [onClick (FilterEvent (Remove ty num))
                    , class "filter-list-item"] 
                    [ text a ]
                    :: (filter_list ty (num+1) res)
            Nothing -> []

filter_dropdown : FilterType -> Int -> List String -> List (Html Msg)
filter_dropdown ty num l = 
    let 
        res = case List.tail l of
            Nothing -> []
            Just x -> x
    in
        case List.head l of 
            Just a -> 
                div [onClick (FilterEvent (Add ty num))
                    , class "filter-dropdown-item"] 
                    [ text a ]
                    :: (filter_dropdown ty (num+1) res)
            Nothing -> []

    

error : LoginData -> Html Msg
error model = 
    let 
        re = Regex.regex "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        error_text = if model.password_again /= model.password then
            Just "Passwords do not match."
        else if Regex.contains re model.mail then
            Nothing
        else
            Just "Please fill in a valid email address."
    in
        case error_text of
            Just x ->
                div [class "error-error"] [ text x ]
            Nothing ->
                div [class "error-fine"] [text "Perfect!"]

