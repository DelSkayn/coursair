module Common.Model exposing (..)
import Http
import LocalStorage
import Common.Sidebar.Part as SideBar 

type Error
    = HttpError Http.Error
    | LocalStorageError LocalStorage.Error

type alias LoginRequest =
    { succesfull : Bool
    , error : Maybe String 
    , session_id : Maybe Int }

type alias RegisterRequest =
    { succesfull : Bool
    , error : Maybe String }

type alias RefreshRequest =
    { succesfull : Bool
    , error : Maybe String }

type alias UserRequest = 
    { id : Int
    , name : String
    , admin : Bool }

type alias LoginData =
    { username : String
    , password : String 
    , mail : String 
    , password_again : String }

type alias Filters = 
    { years : List String
    , languages : List String
    , periodes : List String
    , ecs : List String
    , niveaus : List String }

type alias FilterView =
    { years : Bool
    , languages : Bool
    , periodes : Bool
    , ecs : Bool
    , niveaus : Bool }

type alias Model = 
    { login : LoginData 
    , is_main : Bool
    , search : String
    , id : Maybe Int
    , sidebar : SideBar.Model
    , error : Maybe Error 
    , filter_options : Filters
    , current_filters : Filters
    , filter_view: FilterView}


initial : Bool -> Model
initial is_main = 
    { login = { username = ""
              , password = ""
              , mail = ""
              , password_again = ""}
    , is_main = is_main
    , id = Nothing
    , search = ""
    , sidebar = SideBar.initial
    , error = Nothing 
    , filter_options =
        { years = []
        , languages = []
        , periodes = []
        , ecs = []
        , niveaus = [] }
    , current_filters = 
        { years = []
        , languages = []
        , periodes = []
        , ecs = []
        , niveaus = [] }
    , filter_view = 
        { years = False
        , languages = False
        , periodes = False
        , ecs = False
        , niveaus = False }}

