module Common.Update exposing (..)
import Common.Sidebar.Part as Sidebar
import Common.Model as Model exposing (RegisterRequest, LoginRequest, RefreshRequest, LoginData, Model, Error(..),Filters)
import Http
import Json.Decode exposing (..)
import Json.Encode
import Task exposing (..)
import Debug
import LocalStorage
import String
import Maybe
import Navigation

type FilterType
    = Ec
    | Year
    | Period
    | Niveau
    | Language

type FilterEventType
    = Add FilterType Int
    | Remove FilterType Int
    | Open FilterType
    | Close

type Msg
    = SideBar Sidebar.Msg
    | Noop
    | RequestLogin
    | RequestRegister
    | RequestUser
    | RequestFail Http.Error
    | LoginSucceed LoginRequest
    | RegisterSucceed RegisterRequest
    | RefreshSucceed RefreshRequest
    | UsernameInput String
    | PasswordInput String
    | PasswordAgainInput String
    | MailInput String
    | SessionData (Maybe String)
    | SessionError LocalStorage.Error
    | FilterData Filters
    | SearchInput String
    | Logout
    | FilterEvent FilterEventType


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SideBar msg ->
            let 
                cmd = case msg of
                    Sidebar.Activate x ->
                        case x of
                            Sidebar.Search ->
                                to_root model
                            Sidebar.Filter ->
                                to_root model
                            _ ->
                                Cmd.none
                    _ -> Cmd.none
                side =
                    model.sidebar
            in
                ( { model | sidebar = Sidebar.update msg side }, cmd)

        FilterData x ->
            ({ model | filter_options = x}, Cmd.none)

        RequestLogin -> ( model, login model ) 
        RequestRegister ->
            ( model, register model )

        RequestUser ->
            ( model, Cmd.none )

        Noop ->
            ( model, Cmd.none )

        RequestFail e ->
            ( { model
                | error = Just (HttpError e)
                , id = Nothing
              }
            , Cmd.none
            )

        LoginSucceed r ->
            ( { model
                | id = r.session_id
                , login =
                    { username = ""
                    , password = ""
                    , mail = ""
                    , password_again = ""
                    }
              }
            , case r.session_id of
                Just x ->
                    store_session x

                Nothing ->
                    Cmd.none
            )

        RegisterSucceed _ ->
            ( model, login model )

        RefreshSucceed x ->
            case x.succesfull of
                True ->
                    ( model, Cmd.none )

                False ->
                    ( { model | id = Nothing }, remove_session )

        SessionData x ->
            let
                id =
                    case x of
                        Just y ->
                            case (String.toInt y) of
                                Ok x ->
                                    Just x

                                Err _ ->
                                    Nothing

                        Nothing ->
                            Nothing

                cmd =
                    case id of
                        Just y ->
                            refresh y

                        Nothing ->
                            Cmd.none
            in
                ( { model | id = id }, cmd )

        SessionError e ->
            ( { model | error = Just (LocalStorageError e) }, Cmd.none )

        UsernameInput u ->
            let
                login_data =
                    model.login
            in
                ( { model | login = { login_data | username = u } }, Cmd.none )

        SearchInput i -> 
            ( { model | search = i }, Cmd.none)

        PasswordInput p ->
            let
                login_data =
                    model.login
            in
                ( { model | login = { login_data | password = p } }, Cmd.none )

        MailInput p ->
            let
                login_data =
                    model.login
            in
                ( { model | login = { login_data | mail = p } }, Cmd.none )

        PasswordAgainInput p ->
            let
                login_data =
                    model.login
            in
                ( { model | login = { login_data | password_again = p } }, Cmd.none )

        Logout ->
            let
                id =
                    model.id
            in
                ( { model | id = Nothing }
                , (Cmd.batch
                    [ remove_session
                    , case id of
                        Just x ->
                            logout x

                        Nothing ->
                            Cmd.none
                    ]
                  )
                )

        FilterEvent x ->
            handle_filter_event x model


handle_filter_event : FilterEventType -> Model -> (Model,Cmd Msg)
handle_filter_event x model = 
    case x of 
        Add ty which ->
            let
                opt = model.filter_options
                cur = model.current_filters
            in 
            case ty of
                Ec -> 
                    let 
                        (n_opt,n_cur) = swap_num which opt.ecs cur.ecs
                    in
                        ({model | current_filters = {cur | ecs = n_cur}
                        , filter_options = {opt | ecs = n_opt}}, (to_root model))
                Year -> 
                    let 
                        (n_opt,n_cur) = swap_num which opt.years cur.years
                    in
                        ({model | current_filters = {cur | years = n_cur}
                        , filter_options = {opt | years = n_opt}}, (to_root model))
                Language -> 
                    let 
                        (n_opt,n_cur) = swap_num which opt.languages cur.languages
                    in
                        ({model | current_filters = {cur | languages = n_cur}
                        , filter_options = {opt | languages = n_opt}}, (to_root model))
                Period -> 
                    let 
                        (n_opt,n_cur) = swap_num which opt.periodes cur.periodes
                    in
                        ({model | current_filters = {cur | periodes = n_cur}
                        , filter_options = {opt | periodes = n_opt}}, (to_root model))
                Niveau -> 
                    let 
                        (n_opt,n_cur) = swap_num which opt.niveaus cur.niveaus
                    in
                        ({model | current_filters = {cur | niveaus = n_cur}
                        , filter_options = {opt | niveaus = n_opt}}, (to_root model))
        Remove ty which ->
            let
                opt = model.filter_options
                cur = model.current_filters
            in 
            case ty of
                Ec -> 
                    let 
                        (n_cur,n_opt) = swap_num which cur.ecs opt.ecs
                    in
                        ({model | current_filters = {cur | ecs = n_cur}
                        , filter_options = {opt | ecs = n_opt}}, (to_root model))
                Year -> 
                    let 
                        (n_cur,n_opt) = swap_num which cur.years opt.years
                    in
                        ({model | current_filters = {cur | years = n_cur}
                        , filter_options = {opt | years = n_opt}}, (to_root model))
                Language -> 
                    let 
                        (n_cur,n_opt) = swap_num which cur.languages opt.languages
                    in
                        ({model | current_filters = {cur | languages = n_cur}
                        , filter_options = {opt | languages = n_opt}}, (to_root model))
                Period -> 
                    let 
                        (n_cur,n_opt) = swap_num which cur.periodes opt.periodes
                    in
                        ({model | current_filters = {cur | periodes = n_cur}
                        , filter_options = {opt | periodes = n_opt}}, (to_root model))
                Niveau -> 
                    let 
                        (n_cur,n_opt) = swap_num which cur.niveaus opt.niveaus
                    in
                        ({model | current_filters = {cur | niveaus = n_cur}
                        , filter_options = {opt | niveaus = n_opt}}, (to_root model))
        Open x ->
            case x of
                Year ->
                    ({model | filter_view = 
                        { years = True
                        , languages = False
                        , periodes = False
                        , ecs = False
                        , niveaus = False }}, (to_root model))
                Language ->
                    ({model | filter_view = 
                        { years = False
                        , languages = True
                        , periodes = False
                        , ecs = False
                        , niveaus = False }}, (to_root model))
                Period ->
                    ({model | filter_view = 
                        { years = False
                        , languages = False
                        , periodes = True
                        , ecs = False
                        , niveaus = False }}, (to_root model))
                Ec ->
                    ({model | filter_view = 
                        { years = False
                        , languages = False
                        , periodes = False
                        , ecs = True
                        , niveaus = False }}, (to_root model))
                Niveau ->
                    ({model | filter_view = 
                        { years = False
                        , languages = False
                        , periodes = False
                        , ecs = False
                        , niveaus = True}}, (to_root model))
        Close ->
            ({model | filter_view = 
                { years = False
                , languages = False
                , periodes = False
                , ecs = False 
                , niveaus = False}}, (to_root model))


storage_id_id =
    "session_id"

to_root : Model -> Cmd Msg
to_root model = 
    if model.is_main then
        Cmd.none
    else
        Task.attempt (always Noop)
            (LocalStorage.redirect "/")

check_session : Cmd Msg
check_session =
    let
        which a =
            case a of
                Ok x ->
                    SessionData x

                Err x ->
                    SessionError x
    in
        Task.attempt which
            (LocalStorage.get storage_id_id)


store_session : Int -> Cmd Msg
store_session data =
    let
        which a =
            case a of
                Ok _ ->
                    Noop

                Err x ->
                    SessionError x
    in
        Task.attempt which
            (LocalStorage.set storage_id_id (toString data))


logout : Int -> Cmd Msg
logout id =
    let
        url =
            "data/login"

        body =
            logout_to_json id

        which a =
            case a of
                Ok _ ->
                    Noop

                Err x ->
                    RequestFail x

        req =
            Http.post url (Http.stringBody "application/json" body) (Json.Decode.succeed 0)
    in
        Http.send which req


remove_session : Cmd Msg
remove_session =
    let
        which x =
            case x of
                Ok _ ->
                    Debug.log "Removed session" Noop

                Err y ->
                    Debug.log ("Error while removing session" ++ (toString y)) Noop
    in
        Task.attempt
            (always Noop)
            (LocalStorage.remove storage_id_id)


login : Model -> Cmd Msg
login model =
    let
        url =
            "data/login"

        body =
            login_to_json model.login

        which a =
            case a of
                Ok x ->
                    LoginSucceed x

                Err x ->
                    RequestFail x

        req =
            Http.post url (Http.stringBody "application/json" body) login_data
    in
        Http.send which req


refresh : Int -> Cmd Msg
refresh id =
    let
        url =
            "data/refresh"

        body =
            logout_to_json id

        which a =
            case a of
                Ok x ->
                    RefreshSucceed x

                Err x ->
                    RequestFail x

        req =
            Http.post url (Http.stringBody "application/json" body) refresh_data
    in
        Http.send which req

filters : Cmd Msg
filters = 
    let 
        url = "data/filter"
        request = Http.get url decode_filters
        which a = case a of
            Ok x -> FilterData x
            Err x -> RequestFail x
    in 
        Http.send which request


register : Model -> Cmd Msg
register model =
    let
        url =
            "data/register"

        body =
            register_to_json model.login

        which a =
            case a of
                Ok x ->
                    RegisterSucceed x

                Err x ->
                    RequestFail x

        req =
            Http.post url (Http.stringBody "application/json" body) register_data
    in
        Http.send which req


logout_to_json : Int -> String
logout_to_json id =
    Json.Encode.encode 0
        (Json.Encode.object
            [ ( "session_id", Json.Encode.int id ) ]
        )


login_to_json : LoginData -> String
login_to_json login =
    Json.Encode.encode 0
        (Json.Encode.object
            [ ( "username", Json.Encode.string login.username )
            , ( "password", Json.Encode.string login.password )
            ]
        )


register_to_json : LoginData -> String
register_to_json login =
    Json.Encode.encode 0
        (Json.Encode.object
            [ ( "username", Json.Encode.string login.username )
            , ( "password", Json.Encode.string login.password )
            , ( "mail", Json.Encode.string login.mail )
            ]
        )


login_data : Decoder LoginRequest
login_data =
    Json.Decode.map3 LoginRequest
        (at [ "succesfull" ] bool)
        (maybe (at [ "error" ] string))
        (maybe (at [ "session_id" ] int))


register_data : Decoder RegisterRequest
register_data =
    Json.Decode.map2 RegisterRequest
        (at [ "succesfull" ] bool)
        (maybe (at [ "error" ] string))


refresh_data : Decoder RefreshRequest
refresh_data =
    Json.Decode.map2 RefreshRequest
        (at [ "succesfull" ] bool)
        (maybe (at [ "error" ] string))


decode_filters : Decoder Filters
decode_filters = 
    Json.Decode.map5 Filters
        (at [ "years" ] (list string))
        (at [ "languages" ] (list string))
        (at [ "periodes" ] (list string))
        (at [ "ecs" ] (list string))
        (at [ "niveaus" ] (list string))

encode_filters : Filters -> String 
encode_filters filter = 
    Json.Encode.encode 0
        (Json.Encode.object
            [ ( "years", Json.Encode.list (List.map Json.Encode.string filter.years))
            , ( "languages", Json.Encode.list (List.map Json.Encode.string filter.languages))
            , ( "periodes", Json.Encode.list (List.map Json.Encode.string filter.periodes))
            , ( "ecs", Json.Encode.list (List.map Json.Encode.string filter.ecs))
            , ( "niveaus", Json.Encode.list (List.map Json.Encode.string filter.niveaus))
            ]
        )

swap_num : Int -> List a -> List a -> (List a, List a)
swap_num num from to =
    let
        front = List.take num from
        back = List.drop num from
        new_to = List.head back 
        res_to = case new_to of
            Just x -> x :: to
            Nothing -> to
    in
        (List.append front (rest back),res_to)

rest : List a -> List a
rest list = 
    case List.tail list of
        Just x -> x
        Nothing -> []
