module CourseUpdate exposing(Msg(..),update)
import CourseModel exposing (..)
import Common.Update exposing(Msg(..))
import Common.Sidebar.Part  as Sidebar
import Platform.Cmd exposing (map)
import Task exposing (perform)
import Http exposing (get)
import Task
import Time
import Process
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (decode, required, optional)
import Json.Encode

type Msg 
    = Common Common.Update.Msg
    | Change Show 
    | Like
    | CloseLoginDialog
    | LoginDialogLogin 
    | DelayLogin
    | LikeSucceed LikeResponse
    | IsLikedSucceed IsLikedResponse
    | RequestNewComment
    | CommentInput String
    | CommentClick
    | CommentsSucceed CommentsResponse
    | ReloadComments
    | Noop

type alias LikeRequest =
    { like : Bool
    , session_id : Int
    , course_id : Int }

type alias IsLikedRequest =
    { session_id : Int
    , course_id : Int }

type alias LikeResponse =
    { succesfull : Bool
    , error : Maybe String } 
    
type alias IsLikedResponse =
    { succesfull : Bool
    , error : Maybe String
    , is_liked : Maybe Bool }

type alias CommentsResponse =
    { comments : List Comment }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = 
    case msg of 
        Common com_msg ->
            case com_msg of 
                Common.Update.RequestFail e ->
                    ({model | could_not_retrieve = True, error = Just e }, Cmd.none)
                Common.Update.RefreshSucceed x ->
                    case x.succesfull of
                        True -> 
                            case model.common.id of
                                Just x ->
                                    let 
                                        req = 
                                            { session_id = x
                                            , course_id = model.course.id }
                                    in
                                        (model,is_liked req)
                                Nothing ->
                                    (model,Cmd.none)
                        False ->
                            let 
                                this = model.common
                                (common_model, new_msg) = Common.Update.update com_msg this
                            in 
                                ({model | common = common_model},Cmd.map Common new_msg)
                other ->
                    let 
                        this = model.common
                        (common_model, new_msg) = Common.Update.update com_msg this
                    in 
                        ({model | common = common_model},Cmd.map Common new_msg)
        Change e ->
            case e of 
                Comments ->
                    if List.isEmpty model.comments then
                        ({model | show = e},comments model)
                    else
                        ({model | show = e},Cmd.none)
                _ ->
                    ({model | show = e},Cmd.none)
        Noop ->
            (model,Cmd.none)
        Like ->
            case model.common.id of 
                Just x ->
                    let 
                        data = 
                            { like = not model.user_liked
                            , session_id = x , course_id = model.course.id }
                    in 
                        (model,like data)
                Nothing ->
                    ({model | login_dialog = True} ,Cmd.none)
        LikeSucceed x ->
            let 
                course = model.course
                liked = not model.user_liked
            in
                case x.succesfull of 
                    True ->
                        ({model | user_liked = liked},Cmd.none)
                    False ->
                        (model,Cmd.none)
        IsLikedSucceed x ->
            case x.is_liked of
                Just x ->
                    ({model | user_liked = x},Cmd.none)
                Nothing -> (model,Cmd.none)
        CloseLoginDialog ->
            ({model | login_dialog = False} ,Cmd.none)
        LoginDialogLogin ->
            (model,login_dialog_login)
        DelayLogin ->
            let 
                this = model.common
                msg = Common.Update.SideBar
                        ( Sidebar.Activate Sidebar.Login )
                (common_model, new_msg) = Common.Update.update msg this
            in 
                ({model | common = common_model, login_dialog = False},Cmd.map Common new_msg)
        CommentInput i ->
            ({model | comment = i },Cmd.none)
        RequestNewComment ->
            case model.common.id of
                Just _ ->
                    ({model | comment = ""},comment model)
                Nothing ->
                    ({model | login_dialog = True}, Cmd.none) 
        CommentClick ->
            case model.common.id of
                Just _ ->
                    (model,Cmd.none)
                Nothing ->
                    ({model | login_dialog = True}, Cmd.none) 
        CommentsSucceed comments ->
            ({model | comments = comments.comments}, Cmd.none)
        ReloadComments ->
            (model,comments model)

comments : Model -> Cmd Msg
comments model =
    let 
        url = "data/comments" 
        body = comments_to_json model 
        which a = case a of
            Ok(x) -> CommentsSucceed x
            Err(_) -> Noop 
        req = Http.post url (Http.stringBody "application/json" body) comments_decode
    in
        Http.send which req

login_dialog_login : Cmd Msg
login_dialog_login = 
    Process.sleep (Time.millisecond) 
        |> Task.perform (always DelayLogin)


is_liked : IsLikedRequest -> Cmd Msg
is_liked request = 
    let 
        url = "data/is_liked" 
        body = is_liked_to_json request
        which a = case a of
            Ok(x) -> IsLikedSucceed x
            Err(_) -> Noop 
        req = Http.post url (Http.stringBody "application/json" body) is_liked_decode
    in
        Http.send which req

comment : Model -> Cmd Msg
comment model =
    let
        url = "data/new_comment/"
        body = comment_to_json model
        req = Http.post url (Http.stringBody "application/json" body) like_decode
    in
        Http.send (always ReloadComments) req

comment_to_json : Model -> String
comment_to_json model = 
    Json.Encode.encode 0 ( Json.Encode.object
        [ ("course_id", Json.Encode.int model.course.id)
        , ("session_id", Json.Encode.int (case model.common.id of
            Just x -> x
            Nothing -> -1))
        , ("content", Json.Encode.string model.comment) ])

like : LikeRequest -> Cmd Msg
like request =
    let 
        url = "data/like" 
        body = like_to_json request
        which a = case a of
            Ok(x) -> LikeSucceed x
            Err(_) -> Noop 
        req = Http.post url (Http.stringBody "application/json" body) like_decode
    in
        Http.send which req

like_to_json : LikeRequest -> String
like_to_json req = 
    Json.Encode.encode 0 (Json.Encode.object
        [ ("like", Json.Encode.bool req.like)
        , ("session_id", Json.Encode.int req.session_id)
        , ("course_id", Json.Encode.int req.course_id) ])

is_liked_to_json : IsLikedRequest -> String
is_liked_to_json req = 
    Json.Encode.encode 0 (Json.Encode.object
        [ ("session_id", Json.Encode.int req.session_id)
        , ("course_id", Json.Encode.int req.course_id) ])

comments_to_json : Model -> String
comments_to_json model =
    Json.Encode.encode 0 (Json.Encode.object
        [ ("course_id", Json.Encode.int model.course.id) ])

comments_decode : Decoder CommentsResponse
comments_decode =
    Json.Decode.map CommentsResponse
        (at ["comments"] 
            (Json.Decode.list
                (Json.Decode.map2 Comment
                    (at ["username"] string)
                    (at ["content"] string))))

            

like_decode : Decoder LikeResponse
like_decode = 
    Json.Decode.map2 LikeResponse
        (at ["succesfull"] bool)
        (maybe(at ["error"] string))

is_liked_decode : Decoder IsLikedResponse
is_liked_decode = 
    Json.Decode.map3 IsLikedResponse
        (at ["succesfull"] bool)
        (maybe(at ["error"] string))
        (maybe(at ["is_liked"] bool))

