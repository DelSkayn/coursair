module IndexView exposing (view)
import IndexUpdate exposing(Msg(..))
import IndexModel exposing (Model,CourseData,TagData)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput,onClick, onSubmit)
import Common.View as CommonView


view : Model -> Html Msg
view model = 
    let 
        this = model.common
    in
        CommonView.view this Common [ content model ]

content : Model -> Html msg
content model = 
    ul [class "content-list"] (content_row model.courses)

content_table : List CourseData -> List (Html msg)
content_table courses = 
    if courses == [] then 
        []
    else 
        tr [] (content_row (List.take 3 courses)) 
            :: content_table (List.drop 3 courses)

content_row : List CourseData -> List (Html msg)
content_row courses = 
    let res =
        case List.tail courses of
            Just a -> a
            Nothing -> []
    in
        case List.head courses of
            Just a -> 
                li [] [content_data a]
                    :: content_row res
            Nothing -> []


content_data : CourseData -> Html msg
content_data course = 
    let
        url = "/course?id=" ++ (toString course.id)
    in
    div [ class "course-panel" ] 
        [ div [ class "course-summary" ]
              [ a [ href url ] 
                  [ div [ class "course-header" ]
                        [ text course.name ]]
              , ul [ class "tag-list" ] (tag_list course.tags)
              , course_table course ]]

tag_list : (List TagData) -> List (Html msg )
tag_list tags = 
    let 
        res = case List.tail tags of
            Nothing -> []
            Just x -> x
    in
        case List.head tags of 
            Just a -> 
                li [class "tag-list-item"] [ text a.name ]
                    :: (tag_list res)
            Nothing -> []

course_table : CourseData -> Html msg
course_table course = 
    table [ class "detail-info" ] 
       [ tr [ class "detail-info-language" ] 
            [ td [ class "detail-info-type" ] [ text "language" ]
            , td [ class "detail-info-data" ] [text course.language ]]
       , tr [ class "detail-info-language" ] 
            [ td [ class "detail-info-type" ] [ text "year" ]
            , td [ class "detail-info-data" ] [ text course.year ]]
       , tr [ class "detail-info-language" ] 
            [ td [ class "detail-info-type" ] [ text "period" ]
            , td [ class "detail-info-data" ] [ text course.periode ]]
       , tr [ class "detail-info-language" ] 
            [ td [ class "detail-info-type" ] [ text "teachers" ]
            , td [ class "detail-info-data" ] [ text course.docenten ]]
       , tr [ class "detail-info-language" ] 
            [ td [ class "detail-info-type" ] [ text "EC" ]
            , td [ class "detail-info-data" ] [ text course.ec ]]
       , tr [ class "detail-info-language" ] 
            [ td [ class "detail-info-type" ] [ text "level" ]
            , td [ class "detail-info-data" ] [ text course.niveau ]]]
