import Html
import Common.Update exposing (check_session)
import AddCourseModel exposing (Model,initial)
import AddCourseUpdate exposing (update,Msg(..))
import AddCourseView exposing (view)
import Platform.Cmd

subscriptions : Model -> Sub msg
subscriptions model =
    Sub.none


main = Html.program
    { init = (initial, Cmd.none)
    , update = update
    , view = view
    , subscriptions = subscriptions 
    }
