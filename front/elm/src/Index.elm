import Html
import Html.Events exposing (on)
import IndexView
import Common.Update exposing (check_session,filters)
import IndexModel exposing (Model,initial)
import IndexUpdate exposing (Msg(..),load_courses,update)
import Platform.Cmd

subscriptions : Model -> Sub Msg
subscriptions model = 
    Sub.none


main = Html.program
    { init = (initial, Cmd.batch 
        [ load_courses initial
        , Platform.Cmd.map Common check_session
        , Platform.Cmd.map Common filters ] )
    , update = update
    , view = IndexView.view
    , subscriptions = subscriptions 
    }


