var _user$project$Native_LocalStorage = function() {

    function get (key) {
        return _elm_lang$core$Native_Scheduler.nativeBinding(function(callback) {
            var value = localStorage.getItem(key);
            return callback(_elm_lang$core$Native_Scheduler.succeed(
                        (value === null) ? _elm_lang$core$Maybe$Nothing : _elm_lang$core$Maybe$Just(value)
                        ));
        });
    }

    function set (key, value) {
        return _elm_lang$core$Native_Scheduler.nativeBinding(function(callback) {
            localStorage.setItem(key, value);
            return callback(_elm_lang$core$Native_Scheduler.succeed(value));
        });
    }

    function remove (key) {
        return _elm_lang$core$Native_Scheduler.nativeBinding(function(callback) {
            console.log("Removing: " + key);
            localStorage.removeItem(key);
            return callback(_elm_lang$core$Native_Scheduler.succeed(key));
        });
    }

    function redirect (key) {
        return _elm_lang$core$Native_Scheduler.nativeBinding(function(callback) {
            window.location.href = key;
            return callback(_elm_lang$core$Native_Scheduler.succeed(key));
        });
    }

    /// Massive bug here no idea how to fix it.


    function storageFail () {
        return _elm_lang$core$Native_Scheduler.nativeBinding(function(callback) {
            return callback(_elm_lang$core$Native_Scheduler.fail({ctor: 'NoStorage'}));
        });
    }

    if ('object' !== typeof localStorage) {
        return {
            get: storageFail,
            set: storageFail,
            remove: storageFail,
            redirect: redirect
        };
    }

    return {
        get: get,
        set: F2(set),
        remove: remove,
        redirect: redirect
    };

}();

