module AddCourseView exposing (..)
import AddCourseUpdate exposing (Msg(..))
import AddCourseModel exposing (Model)
import Html exposing (..)
import Html.Events exposing (onClick,onInput)
import Html.Attributes exposing (..)

view : Model -> Html Msg
view model =
    div [ class "wrap" ]
        [ p [] [text "select course, course id:"]
        , input [onInput Select] []
        , div [ class "form-wrap" ]
              [ p [] [text "name"]
              , textarea [ onInput Name, value model.current_course.name ] []
              , p [] [text "language"]
              , textarea [ onInput Language, value model.current_course.language] []
              , p [] [text "period"]
              , textarea [ onInput Periode, value model.current_course.periode] []
              , p [] [text "year"]
              , textarea [ onInput Year,value model.current_course.year] []
              , p [] [text "teachers"]
              , textarea [ onInput Docenten, value model.current_course.docenten] []
              , p [] [text "ec"]
              , textarea [ onInput Ec,value model.current_course.ec] []
              , p [] [text "level"]
              , textarea [ onInput Niveau,value model.current_course.niveau] []
              , p [] [text "discription"]
              , textarea [ onInput Discription,value model.current_course.discription] []
              , p [] [text "summary, not used currently"]
              , textarea [ onInput Summary,value model.current_course.summary] []
              , div [class "button", onClick Submit] [text "submit changes"]
              , div [class "button", onClick NewCourse] [text "add new course"]]]

