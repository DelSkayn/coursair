CREATE TABLE courses(
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE,
    language VARCHAR NOT NULL,
    periode VARCHAR NOT NULL,
    year VARCHAR NOT NULL,
    docenten VARCHAR NOT NULL,
    ec VARCHAR NOT NULL,
    niveau VARCHAR NOT NULL,
    summary TEXT,
    discription TEXT
);


CREATE TABLE tags(
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE,
    explenation TEXT
);

CREATE TABLE has_tags(
    id SERIAL PRIMARY KEY,
    tag_id INT REFERENCES tags NOT NULL,
    course_id INT REFERENCES courses NOT NULL,
    strength INT NOT NULL
);

CREATE TABLE users( 
    id SERIAL PRIMARY KEY, 
    name VARCHAR NOT NULL UNIQUE,
    admin BOOLEAN NOT NULL,
    password BIGINT NOT NULL,
    salt BIGINT NOT NULL
);

CREATE TABLE users_likes(
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users NOT NULL,
    course_id INT REFERENCES courses NOT NULL,
    UNIQUE(user_id,course_id)
);

CREATE TABLE comments(
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users,
    course_id INT REFERENCES courses,
    content TEXT NOT NULL
);

CREATE TABLE teachers(
    id SERIAL PRIMARY KEY,
    surname VARCHAR NOT NULL UNIQUE,
    family_name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE gives_courses(
    id SERIAL PRIMARY KEY,
    teacher_id INT REFERENCES teachers,
    course_id INT REFERENCES courses
);


INSERT INTO courses (name, language, periode, year, docenten, ec, niveau, summary, discription)
    VALUES(
          'Human Computer Interaction'
        , 'English / dutch'
        , '1st semester' 
        , '2016-2017'
        , 'dr.ir. F.J.Verbeek'
        , '6.0'
        , '400'
        , 'The best course there is in the whole world'
        , '# Admission requirements
Not applicable

# Description
Human Computer Interaction is concerned with man-machine interfaces. Every system equipped with a microprocessor has some kind of user interface for its operation. This is, in particularly, the case for systems that require which interaction as an essential ingredient for its normal operation: i.e. computers and computer programs.
Human Computer Interaction covers various aspects of the interaction between the human operator a computer system. In the lectures the underlying principles for the design of the computer interface and interaction are discussed. This includes aspects of human perception, cognitive processes and memory but also subjects directly related to interface and interaction design, i.e. metaphores, widgets, windowing systems and object orientation. In the modern approach of Human Computer Interaction, the user is the pivot of the design trajectory. Design methods are based on this principle and this will be clear in discussion of problem analysis, prototyping, evaluation and usability. Recent developments in HCI are discussed in the lectures.
The students in this course are taking part of either the regular Computer Science (CS, including I&E) programme or of the Mediatechnology (MT) programme. In addition, the course is included in the minor Computer Science. The course consists of two parts: (1) HCI Theory, and (2) practical assignments. Documentation and assignment is made available via the website of this course.

# Course objectives
Understand the major principles of interaction design. Understand the key concepts in the trajectory of designing and implementing interactive products. Being able to apply these concepts to a practical research plan and to study the usability of an interactive application. Being able to critically assess the design process through a research question and report on results of a “short” study.

# Time table
The most recent version of the schedule can be found on the Liacs website

# Mode of instruction
In the first part of the semester, twice a week a lecture of 2 hours. After the lecture series is completed (mid-October) the practical part will be monitored; students work in a each team of two and will have to present their project as well as their progress. This is considered beneficial for all students working on the projects, presentation skills and feedback, as well as to the other students, exercise in critical evaluation and inspiration for their own projects. The presentations are scheduled at the same time as the normal lecture-hours. Appointments regarding the presentation date will be made after evaluation of your work plan. The composition of a workgroup is made such to stimulate active participation of all students attending.

# Assessment method
Theoretical concepts as presented in the lectures are tested in a written exam. This exam contributes 35% to the course grade. Most of the course is “hands-on”, i.e. students will design, implement, evaluate and present their own interactive products. In addition to the design of the interface, research on its usability is an essential part of the practical work. For the practical part attendance of workgroup presentations is obligatory according to the arrangements presented by the lecturer. A short paper is presented on the assignment and the process to the final product. The grade for the practical work is evaluated after a final presentation. The grade is composed of a number of assessments that are indicative for the overall quality of the practical work. The practical work contributes 65% to the final grade. Both parts of the final grade should be >= 5.5.

# Reading list
The recommended book that will be used throughout the course and provides a lot of background for the assignment is: Designing Interactive Systems (2nd edition) David Benyon, 2010, Pearson-Addison Wesley
The book can be ordered via Benyon

# Other Books:
* Beyond Human Computer Interaction – Jenny Preece, et al., 2002, Wiley and Sons.
* Designing the User Interface – Ben Shneiderman, 1998, AWL
* The Human Interface – Jef Raskin, 2002, AWL
* Human Computer Interaction – Jenny Preece et al, 1995, AWL

# Registration
Aanmelden via Usis: Selfservice > Sudentencentrum > Inschrijven
Activiteitencodes te vinden via de facultaire website

# Contact information
Study coordinator Informatica: Riet Derogee

# Website
HCI');

INSERT INTO courses (name, language, periode, year, docenten, ec, niveau, summary, discription)
    VALUES('Compiler Construction'
        , 'Dutch'
        , '1st semester'
        , '2016-2017'
        , 'Ehm'
        , '6.0'
        , '400'
        , 'The course with the best abbrivation'
        , 'Seriously Coco is just a funny abbrivation.');

INSERT INTO courses (name, language, periode, year, docenten, ec, niveau, summary, discription)
    VALUES('Computer Graphics'
        , 'Dutch'
        , '1st semester'
        , '2016-2017'
        , 'What'
        , '6.0'
        , '420'
        , 'Learn the computer graphics'
        , 'Vertecies and shit.');

INSERT INTO courses (name, language, periode, year, docenten, ec, niveau, summary, discription)
    VALUES('Another test course '
        , 'Dutch'
        , '1st semester'
        , '2016-2017'
        , 'What'
        , '6.0'
        , '420'
        , 'Learn the computer graphics'
        , 'Vertecies and shit.');


INSERT INTO tags (name, explenation)
    VALUES('Awesome','The best courses');

INSERT INTO tags (name, explenation)
    VALUES('Needs A Computer','Dont all course need a computor');

INSERT INTO has_tags (tag_id,course_id,strength)
    VALUES(
        (SELECT id from tags where name = 'Awesome'),
        (SELECT id from courses where name = 'Human Computer Interaction'),
        0
    );

INSERT INTO has_tags (tag_id,course_id,strength)
    VALUES(
        (SELECT id from tags where name = 'Awesome'),
        (SELECT id from courses where name = 'Compiler Construction'),
        0
    );

INSERT INTO has_tags (tag_id,course_id,strength)
    VALUES(
        (SELECT id from tags where name = 'Needs A Computer'),
        (SELECT id from courses where name = 'Compiler Construction'),
        0
    );
